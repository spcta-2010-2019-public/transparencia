# README

## Instalación

Conexión a internet funcionando. Se necesita para la instalación de las gemas y ruby.

Instalación de PostgreSQL funcionando. Más información: https://www.postgresql.org/download/

Instalación de RVM, funcionando. Más información: https://rvm.io/rvm/install

Desde una terminal se accede al directorio del proyecto de la siguiente forma:

`cd sgs`

Luego se ejecutan los siguientes comandos:

`rvm install ruby-2.3.0`

`rvm use 2.3.0`

`rvm gemset create sgs`

`rvm use 2.3.0@sgs`

`gem install bundler`

`bundle install`

Crear una copia de `database.yml.sample`, en config/ llamarla `database.yml` y modificar la información necesaria para la conexión a la BD.

`rake db:create`

`rake db:migrate`

`rake menu:load`

Utilizando el comando `rails c` desde una terminal y en la raiz del proyecto, se deberá crear un nuevo "Admin" con role = "admin", de la siguiente forma:

`Admin.new(email: "correoelectronico@example.com", name: "ANONYMOUS", role: "admin", password: "contraseña", password_confirmation: "contraseña").save`

Ingresar al proyecto desde un navegador y dentro del namespace "/panel" (http://localhost:3000/panel), ingresar con las credenciales creadas para el Admin creado anteriormente.

Para iniciar el proyecto se introduce el introduce el comando:

`rails s`
