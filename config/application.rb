# frozen_string_literal: true
require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TransparenciaSv
  #
  class Application < Rails::Application
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.time_zone = 'Central America'
    config.i18n.default_locale = :es
    config.autoload_paths << Rails.root.join('app').join('pdf')
  end
end

Config = YAML.load_file("#{Rails.root}/config/config.yml").with_indifferent_access
