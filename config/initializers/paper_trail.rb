# frozen_string_literal: true
PaperTrail.config.track_associations = false
PaperTrail::Rails::Engine.eager_load!
