# frozen_string_literal: true
Chartkick.options = {
  height: '200px',
  colors: ['#357bf3', '#2c51ab', '#43be6a', '#515ba6']
}
