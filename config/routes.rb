# frozen_string_literal: true
Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  #
  devise_for :admins, controllers: { sessions: 'admins/sessions' }

  %w(404 422 500 503).each do |code|
    get code, to: 'errors#show', code: code
  end

  root to: 'home#index'

  resources :categories, only: [:show]

  resources :institutions,                only: [:show] do
    resources :documents,                 only: [:show] do
      member do
        get :download
      end
    end
    resources :remunerations,             only: [:index]
    resources :selections,                only: [:index]
    resources :inventories,               only: [:index, :show]
    resources :officials,                 only: [:index, :show]
    resources :consultants,               only: [:index, :show]
    resources :travels,                   only: [:index, :show]
    resources :services,                  only: [:index, :show]
    resources :privates,                  only: [:index, :show]
    resources :executing_works,           only: [:index, :show]
    resources :contracts,                 only: [:index, :show]
    resources :recruitment_results,       only: [:index, :show]
    resources :participation_mechanisms,  only: [:index, :show]
  end

  namespace :panel do
    root to: 'activity_logs#index'
    get "institution_logs" => "activity_logs#institution_logs", as: :institution_logs

    resources :activity_logs, only: [:index, :show]
    resources :profile, only: [:edit, :update]
    resources :versions, only: [:show]

    resources :admin_settings, only: [] do
      put :table_columns, on: :collection
    end

    resources :recycler_docks, only: [:index, :show] do
      get :restore, on: :member
    end

    [
      :oir_addresses,
      :institution_types,
      :institutions,
      :institution_dependencies,
      :executing_works,
      :standard_categories,
      :document_categories,
      :information_standard_frames,
      :information_standards,
      :travels,
      :committees,
      :officials,
      :consultants,
      :budgetary_units,
      :services_categories,
      :services,
      :service_steps,
      :work_lines,
      :remunerations,
      :admins,
      :institution_items,
      :procurements,
      :institution_recruitment_results,
      :resources_to_private_recipients,
      :participation_mechanisms,
      :institution_confidential_infos,
      :passage_payers,
      :viatical_payers,
      :expenses_payers,
      :disclaimers,
      :execution_phases
    ].each do |resource|
      resources resource do
        put :updates, on: :collection
        put :destroys, on: :collection
        get :list, on: :collection
      end
    end

    resources :statistics, only: [:index] do
      post :standards_downloaded, on: :collection
      post :documents_downloaded, on: :collection
    end

    resources :documents do
      get :information_standard_document_categories, on: :collection
      put :updates, on: :collection
      put :destroys, on: :collection
      get :list, on: :collection
    end

    namespace :grid do
      [
        :oir_addresses,
        :institution_types,
        :institutions,
        :institution_dependencies,
        :executing_works,
        :standard_categories,
        :document_categories,
        :information_standard_frames,
        :information_standards,
        :travels,
        :committees,
        :officials,
        :consultants,
        :documents,
        :budgetary_units,
        :services_categories,
        :services,
        :service_steps,
        :work_lines,
        :remunerations,
        :admins,
        :institution_inventory_categories,
        :institution_items,
        :procurements,
        :institution_recruitment_results,
        :institution_confidential_infos,
        :resources_to_private_recipients,
        :participation_mechanisms
      ].each do |resource|
        resources resource, only: [:index, :new, :create]
      end
    end
  end

  namespace :api do
    namespace :v1 do
      resources :home, only: [:index]
      resources :institution_types, only: [:index]
      resources :institutions, only: [:index]
      resources :standard_categories, only: [:index]
      resources :information_standards, only: [:index]
      resources :information_standard_frames, only: [:index]
      resources :consultants, only: [:index]
      resources :committees, only: [:index]
      resources :budgetary_units, only: [:index]
      resources :dependencies, only: [:index]
      resources :disclaimers, only: [:index]
      resources :document_categories, only: [:index] #
      resources :documents, only: [:index]           #
      resources :executing_works, only: [:index]
      resources :inventories, only: [:index]
      resources :officials, only: [:index]
      resources :participation_mechanisms, only: [:index] #
      resources :privates, only: [:index]
      #resources :recruitment_results, only: [:index] #
      resources :remunerations, only: [:index]
      resources :services_categories, only: [:index]
      resources :services, only: [:index]
      resources :service_steps, only: [:index]
      resources :travels, only: [:index]
      resources :documents, only: [:index]
      resources :document_categories, only: [:index]
    end
  end

  get 'search', to: 'home#search'
end
