# frozen_string_literal: true
#
class RemunerationsController < ApplicationController
  include Componentable
  include PdfExportable
  include CsvExportablePublic

  def model
    Remuneration
  end

  def index
    super
    @disclaimers                      = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                        = {}
    params[:q][:year_cont]            = params[:rm_year] unless params[:rm_year].blank?
    params[:q][:name_cont]            = params[:rm_name] unless params[:rm_name].blank?
    params[:q][:salary_category_cont] = params[:rm_salary_category] unless params[:rm_salary_category].blank?
    params[:q][:work_line_id_eq     ] = params[:rm_work_line_id] unless params[:rm_work_line_id].blank?
    @rm_years                         = @items.enabled.collect{|m| m.year}.uniq.delete_if{|i|i==0} rescue nil
    @rm_work_lines                    = @items.enabled.reject{|n| n.work_line.nil?}.map{|a| [a.work_line.name, a.work_line.id]}.uniq.compact
    @show_form                        = @items.count > 0
    @fixed_params                     = search_params(params)
    @q                                = @items.enabled.search(params[:q])
    @results                          = @q.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:institution_remunerations])
    @official             = Official.find(params[:id]) if action_name == "show" rescue nil
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_remunerations], @institution.id).order("name ASC")

    if @information_standard.priority_order?
      @documents = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_remunerations], @institution.id).reorder("year DESC, priority DESC")
    else
      @documents = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_remunerations], @institution.id).order('updated_at desc')
    end

    @years                = @documents.reorder("year ASC").map{|y| y.year}.uniq.delete_if{|y| y.to_i==0}
  end

  def breadcrumb
    super
    add_breadcrumb 'Remuneraciones', institution_remunerations_url(@institution)
  end

  def exportable_fields
    [:institution_id, :work_line_id, :year, :name, :employees_number,
     :ad_honorem, :monthly_remuneration, :expenses_remuneration,
     :salary_category]
  end

  def permits
    [:institution_id, :work_line_id, :active, :enabled, :marked, :ad_honorem,
     :name, :employees_number, :monthly_remuneration, :expenses_remuneration,
     :salary_category, :created_at, :updated_at, :year]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
