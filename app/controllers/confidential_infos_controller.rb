# frozen_string_literal: true
# YA NO VA
class ConfidentialInfosController < ApplicationController
=begin
  include Componentable
  include PdfExportable

  def model
    InstitutionConfidentialInfo
  end

  def index
    super
    params[:doc] = {}
    if params[:updated_at].to_i > 0
      params[:doc][:updated_at_lteq] = Date.new(params[:updated_at].to_i).end_of_year
      params[:doc][:updated_at_gteq] = Date.new(params[:updated_at].to_i)
    end
    params[:doc][:name_or_description_cont] = params[:name_or_description] unless params[:name_or_description].nil?
    params[:doc][:document_category_id_eq] = params[:document_category].to_i unless params[:document_category].nil?
    @docs = @documents.search(params[:doc])
    @doc_items = @docs.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 30)
  end

  def show
    respond_to do |format|
      format.html
      format.pdf { super }
    end
  end

  def filters
    super
    @confidential_info = InstitutionConfidentialInfo.find(params[:id]) if action_name == 'show'
    @document_categories = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_confidential_infos], @institution.id).order("name ASC")
    @documents = Document.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_confidential_infos], @institution.id).order('updated_at desc')
    @years = @documents.map{|y| y.updated_at.year}.uniq

  rescue
    nil
  end

  def breadcrumb
    super
    add_breadcrumb 'Recursos destinados a privados', institution_confidential_infos_url(@institution)
    add_breadcrumb truncate(@confidential_info.name, length: 50),
                   institution_service_url(@institution, @confidential_info)
  rescue
    nil
  end

=end
end
