# frozen_string_literal: true
#
class ConsultantsController < ApplicationController
  include PdfExportable
  include Componentable
  include CsvExportablePublic

  def model
    Consultant
  end

  def index
    super
    @disclaimers                              = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                                = {}
    params[:q][:name_cont]                    = params[:consultants_name] unless params[:consultants_name].blank?
    params[:q][:assigned_unit_cont]           = params[:assigned_unit] unless params[:assigned_unit].blank?
    params[:q][:active_eq]                    = params[:consultants_active] unless params[:consultants_active].blank?
    params[:q][:institution_dependency_id_eq] = params[:consultants_institution_dependency_id] unless params[:consultants_institution_dependency_id].blank?
    @show_form                                = @items.count > 0
    @fixed_params                             = search_params(params)
    @q                                        = @items.enabled.search(params[:q])
    @results                                  = @q.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:institution_consultants])
    @consultant           = Consultant.find(params[:id]) if action_name == "show" rescue nil
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_consultants], @institution.id).order("priority ASC")
    @documents            = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_consultants], @institution.id).order('updated_at desc')
    @years                = @documents.enabled.where('year > 0').reorder("year ASC").pluck('distinct(year)').sort
    @assigned_units       = Consultant.pluck(:assigned_unit).uniq

  end

  def breadcrumb
    super
    add_breadcrumb 'Asesores', institution_consultants_url(@institution)
    add_breadcrumb truncate(@consultant.name, :length => 50), institution_official_url(@institution, @consultant) rescue nil
  end

  def exportable_fields
    [:institution_id, :institution_dependency_id, :name, :position,
    :remuneration, :phone, :email, :description, :applied_studies,
    :work_experience, :functions]
  end

  def permits
    [:institution_id, :institution_dependency_id, :enabled, :active, :marked,
     :name, :position, :remuneration, :phone, :email, :description,
     :applied_studies, :work_experience, :functions, :assigned_unit]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.enabled.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
