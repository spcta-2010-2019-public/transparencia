# frozen_string_literal: true
#
class OfficialsController < ApplicationController
  include PdfExportable
  include Componentable
  include CsvExportablePublic

  def model
    Official
  end

  def index
    super
    @disclaimers                                = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                                  = {}
    params[:q][:name_or_position_or_email_cont] = params[:officials_name_or_position_or_email] unless params[:officials_name_or_position_or_email].blank?
    params[:q][:active_eq]                      = params[:officials_active] unless params[:officials_active].blank?
    params[:q][:institution_dependency_id_eq]   = params[:officials_institution_dependency_id] unless params[:officials_institution_dependency_id].blank?
    @show_form                                  = @items.count > 0
    @fixed_params                               = search_params(params)
    @q                                          = @items.enabled.search(params[:q])
    @results                                    = @q.result.order("priority ASC, name ASC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:institution_officials])
    @official             = Official.find(params[:id]) if action_name == "show" rescue nil
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_officials], @institution.id).order("name ASC")
    @documents            = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_officials], @institution.id).order('updated_at desc')
    @years                = @documents.where('year > 0').reorder("year ASC").pluck('distinct(year)').sort
  end

  def breadcrumb
    super
    add_breadcrumb 'Funcionarios', institution_officials_url(@institution)
    add_breadcrumb truncate(@official.name, :length => 50), institution_official_url(@institution, @official) rescue nil
  end

  def exportable_fields
    [:institution_id, :institution_dependency_id, :committee_id, :name, :phone,
     :email, :position, :address, :curriculum]
  end

  def permits
    [:institution_id, :institution_dependency_id, :committee_id, :enabled,
     :active, :marked, :name, :position, :phone, :email, :address, :functions,
     :curriculum, :created_at, :updated_at, :priority]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.enabled.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
