# frozen_string_literal: true
# rubocop:disable ClassLength
class DocumentsController < ApplicationController
  before_action :init_show, only: [:show]

  def show
    return kick_out!         unless institution.enabled?
    redirect_to friendly_url unless friendly_url?

    @q     = @documents.search(params[:q])
    @items = @q
             .result
             .order('updated_at DESC')
             .paginate(page: params[:page], per_page: 30)
  end

  def download
    if Document.exists? params[:id]

      document = Document.find params[:id]
      document.update_column(:downloads, (document.downloads + 1))
      send_file(*downloable_attrs(document))
    else
      render text: 'No encontrado'
    end
  end

  def downloable_attrs(document)
    [
      document.document.path,
      filename: document.document_file_name,
      type: document.document_content_type,
      disposition: 'attachment'
    ]
  end

  def kick_out!
    redirect_to category_url(@institution.institution_type)
  end

  def friendly_url?
    @information_standard.friendly_id == params[:id] &&
      @institution.friendly_id == params[:institution_id]
  end

  def friendly_url
    url_for(
      action: action_name,
      institution_id: institution.friendly_id,
      id: information_standard.friendly_id,
      status: 301
    )
  end

  def init_show
    institution
    information_standard
    institution_standards
    document_categories
    documents
    years
    disclaimers
    build_breadcrumb
  end

  def institution
    @institution = Institution.friendly.find params[:institution_id]
  end

  def information_standard
    @information_standard = InformationStandard.friendly.find params[:id]
  end

  def institution_standards
    @institution_standards = institution
                             .standard_category
                             .information_standards
                             .order(:information_standard_frame_id, :priority)
                             .group_by(&:information_standard_frame_id)
  end

  def document_categories
    conditions = [
      'information_standard_id = ? AND institution_id = ?',
      information_standard.id,
      institution.id
    ]
    @document_categories = DocumentCategory
                           .where(conditions)
                           .order(:name)
  end

  def disclaimers
    @disclaimers = Disclaimer
                   .enabled
                   .where(
                     institution_id: institution.id,
                     information_standard_id: information_standard.id
                   )
  end

  def documents_order
    return nil unless @information_standard.priority_order?
    { year: :desc, priority: :desc }
  end

  def documents_institution
    i = @institution.shared_institution_standard(@information_standard)
    i = @institution if i.nil?
    i
  end

  def documents
    @documents = Document
                 .enabled
                 .where(
                   institution: documents_institution,
                   information_standard_id: @information_standard
                 )
                 .reorder(documents_order)
  end

  def years
    @years = @documents
             .where('year > 0')
             .reorder(:year)
             .pluck('distinct(year)')
             .sort
  end

  def build_breadcrumb
    add_breadcrumb 'Inicio', root_url

    add_breadcrumb @institution.institution_type.name,
                   category_url(@institution.institution_type)

    add_breadcrumb @institution.name, institution_url(@institution)

    add_breadcrumb @information_standard.name,
                   institution_document_url(@institution, @information_standard)
  end
end
