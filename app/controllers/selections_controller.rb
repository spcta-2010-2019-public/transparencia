# frozen_string_literal: true
#
class SelectionsController < ApplicationController
  include Componentable

  def model
    InstitutionRecruitmentResult
  end

  def index
    @institution          = Institution.friendly.find params[:institution_id]
    @information_standard = InformationStandard.find Config[:dynamical_information_standards][:institution_recruitment_process]

    i = @institution.shared_institution_standard(@information_standard) unless @information_standard.nil?
    @institution = i unless i.nil?


    @items       = model.where(institution_id: @institution.id, enabled: true)

    params[:doc] = {}
    if params[:documents_updated_at].to_i > 0
      params[:doc][:updated_at_lteq] = Date.new(params[:documents_updated_at].to_i).end_of_year
      params[:doc][:updated_at_gteq] = Date.new(params[:documents_updated_at].to_i)
    end
    params[:doc][:name_or_description_cont] = params[:documents_name_or_description]    unless params[:documents_name_or_description].blank?
    params[:doc][:document_category_id_eq]  = params[:documents_document_category].to_i unless params[:documents_document_category].blank?

    @docs                  = @documents.search(params[:doc])
    @doc_items             = @docs.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 30)

    # Resultados de contratación
    params[:s]             = {}
    params[:s][:name_cont] = params[:recruitment_name] unless params[:recruitment_name].blank?
    @show_form2            = @items.count > 0
    @qresults              = @items.search(params[:s])
    @results               = @qresults.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 20)

    # Procedimientos de contratación
    @institution_standards = @institution.standard_category.information_standards.order(:information_standard_frame_id, :priority).group_by { |t| t.information_standard_frame_id }
    @document_categories   = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", @information_standard.id, @institution.id).order("name ASC")

    if @information_standard.priority_order?
      @documents           =  Document.enabled.where(:institution => @institution, :information_standard_id => @information_standard).reorder("year DESC, priority DESC")
    else
      @documents           =  Document.enabled.where(:institution => @institution, :information_standard_id => @information_standard)
    end

    @show_form             = @documents.count > 0
    @years                 = @documents.where('year > 0').reorder('year ASC').pluck('DISTINCT CAST(year AS INT)').sort
    @qprocs                = @documents.search(params[:q])
    @items                 = @qprocs.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 30)
    @disclaimers           = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:institution_recruitment_results])
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_recruitment_results], @institution.id).order("name ASC")
    @documents            = Document.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_recruitment_results], @institution.id).order('updated_at desc')
    @years_selection      = @documents.map{|y| y.updated_at.year}.uniq
  end

  def breadcrumb
    super
    add_breadcrumb 'Resultado de selección y contratación de personal', institution_recruitment_results_url(@institution)
  end

end
