# frozen_string_literal: true
#
class InstitutionsController < ApplicationController
  def show
    @institution = Institution.friendly.find params[:id]
    redirect_to action: action_name, id: @institution.friendly_id, status: 301 unless @institution.friendly_id == params[:id]

    unless @institution.enabled?
      redirect_to category_url(@institution.institution_type) and return
    end

    add_breadcrumb 'Inicio', root_url
    add_breadcrumb @institution.institution_type.name, category_url(@institution.institution_type)
    add_breadcrumb @institution.name, institution_url(@institution)

    #@institution_standards = @institution.standard_category.information_standards.order(:information_standard_frame_id, :priority).group_by { |t| t.information_standard_frame_id }
    #PARA ESTANDAR DE COMISIONADOS (temporal)
    institution_array = [9, 14, 16, 24] #CAPRES; MIGOBDT; MTPS; MINED
    if institution_array.include?(@institution.id)
      @institution_standards = @institution.standard_category.information_standards.order(:information_standard_frame_id, :priority).group_by { |t| t.information_standard_frame_id }
    else
      #Sustituir id por el estandar que corresponde al de eleccion de comisionados
      @institution_standards = @institution.standard_category.information_standards.where.not("information_standards.id = ?", 51).order(:information_standard_frame_id, :priority).group_by { |t| t.information_standard_frame_id }
    end

    @services_number =       @institution.services.count rescue 0

    @documents_number =      @institution.documents.count rescue 0
    @documents_downloads =   @institution.documents.sum(:downloads) rescue 0
    @most_downloaded_docs =  @institution.documents.reorder("downloads DESC").limit(4)
    @marked_documents =      @institution.documents.marked.reorder("updated_at DESC").limit(3)

    @coords =                @institution.oir_addresses.first

    @information_standard_top_downloaded = [['Estandar', 'Descargas']]
    @information_standard_top_downloaded_info = InformationStandard.
      select("information_standards.*, SUM(documents.downloads) AS downloads").
      joins('LEFT JOIN documents ON (documents.information_standard_id = information_standards.id)').
      where('documents.institution_id = ?', @institution.id).
      group('information_standards.id').
      reorder('downloads DESC').
      limit(4)

    @information_standard_top_downloaded_info.each do |information_standard|
      @information_standard_top_downloaded << [information_standard.name, information_standard.downloads.to_i]
    end
  end
end
