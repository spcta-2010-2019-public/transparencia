# frozen_string_literal: true
#
class ContractsController < ApplicationController
  include PdfExportable
  include Componentable
  include CsvExportablePublic

  def model
    Procurement
  end

  def index
    super
    @disclaimers                                                                         = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                                                                           = {}
    params[:q][:institutional_area_cont]                                                 =  params[:contracts_institutional_area] unless  params[:contracts_institutional_area].nil?
    params[:q][:counterpart_name_or_name_or_institutional_area_or_form_of_contract_or_adquisition_code_or_compliance_deadlines_or_contract_code_or_amount_cont] = params[:contracts_counterpart_name_or_name] unless params[:contracts_counterpart_name_or_name].nil?
    @contract_years = @items.where('year > 0').order('year ASC').pluck('DISTINCT CAST(year AS INT)').sort
    @show_form      = @items.count > 0
    @areas          = @items.where('institutional_area IS NOT NULL').pluck('DISTINCT institutional_area').sort
    @items          = @items.where('year = ?', params[:contracts_date].to_i) unless params[:contracts_date].blank? rescue nil
    @fixed_params   = search_params(params)
    @q              = @items.enabled.search(params[:q])
    @results        = @q.result.order("year DESC, priority DESC, name ASC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:institution_procurements])
    @contract             = Procurement.find(params[:id]) if action_name == "show" rescue nil
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_procurements], @institution.id).order("name ASC")

    if @information_standard.priority_order?
      @documents         =  Document.enabled.where(:institution_id => @institution.id, :information_standard_id => Config[:dynamical_information_standards][:institution_procurements]).reorder("year DESC, priority DESC")
    else
      @documents         =  Document.enabled.where(:institution_id => @institution.id, :information_standard_id => Config[:dynamical_information_standards][:institution_procurements]).order('updated_at desc')
    end

    @years                = @documents.where('year > 0').reorder("year ASC").pluck('DISTINCT year').sort
  end

  def breadcrumb
    super
    add_breadcrumb 'Contrataciones y adquisiciones', institution_contracts_url(@institution)
    add_breadcrumb truncate(@consultant.name, :length => 50), institution_contract_url(@institution, @consultant) rescue nil
  end

  def exportable_fields
    [:institution_id, :name, :institutional_area, :adquisition_code,
     :amount, :counterpart_name, :counterpart_info, :compliance_deadlines,
     :start_date, :form_of_contract, :contract_code, :attachment_url]
  end

  def permits
    [:institution_id, :name, :amount, :form_of_contract, :counterpart_info,
     :compliance_deadlines, :attachment, :start_date, :counterpart_name,
     :adquisition_code, :institutional_area, :contract_code, :enabled]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
