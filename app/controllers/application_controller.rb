# frozen_string_literal: true
#
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action { set_meta_tags site_meta_tags }

  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
    return panel_root_url if resource_or_scope == :admin
    super
  end

  def site_meta_tags
    {
      title: 'Portal de Transparencia - El Salvador',
      description: 'Consulta y solicita información pública de Presidencia, ministerios, entidades autónomas, gobernaciones departamentales, hospitales y alcaldías.',
      canonical: request.original_url,
      og: og_meta_tags

    }
  end
  helper_method :site_meta_tags

  def og_meta_tags
    {
      title: 'Portal de Transparencia - El Salvador',
      url: request.original_url,
      description: 'Consulta y solicita información pública de Presidencia, ministerios, entidades autónomas, gobernaciones departamentales, hospitales y alcaldías.',
      image: {
        _: view_context.image_url('gimmick.png'),
        width: 1200,
        height: 630
      }
    }
  end
end
