# frozen_string_literal: true
#
class PrivatesController < ApplicationController
  include PdfExportable
  include Componentable
  include CsvExportablePublic

  def model
    ResourcesToPrivateRecipient
  end

  def index
    super
    @disclaimers                                      = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                                        = {}
    params[:q][:name_or_allocation_of_resources_cont] = params[:privates_fields] unless params[:privates_fields].blank?
    params[:q][:institution_dependency_id_eq]         = params[:privates_institution_dependency_id] unless params[:privates_institution_dependency_id].blank?
    @show_form                                        = @items.count > 0
    @privates_years                                   = @items.where('period_begin IS NOT NULL').pluck("DISTINCT CAST(EXTRACT(year from period_begin) AS INT)").sort
    @items                                            = @items.where("period_begin BETWEEN ? AND ?", Date.new(params[:privates_date].to_i), Date.new(params[:privates_date].to_i).end_of_year) unless params[:privates_date].blank? rescue nil
    @fixed_params                                     = search_params(params)
    @q                                                = @items.enabled.search(params[:q])
    @results                                          = @q.result.order("updated_at DESC, priority ASC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:resources_to_private_recipients])
    @resource             = ResourcesToPrivateRecipient.find(params[:id]) if action_name == 'show'
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:resources_to_private_recipients], @institution.id).order("name ASC")
    @documents            = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:resources_to_private_recipients], @institution.id).order('updated_at desc')
    @years                = @documents.reorder("year ASC").map{|y| y.year}.uniq.delete_if{|y| y.to_i==0}
  rescue
    nil
  end

  def breadcrumb
    super
    add_breadcrumb 'Recursos públicos destinados a privados', institution_resources_url(@institution)
    add_breadcrumb truncate(@resource.name, length: 50),
    institution_service_url(@institution, @resource)
  rescue
    nil
  end

  def exportable_fields
    [:institution_id, :institution_dependency_id, :name,
     :allocation_of_resources, :resources_amount, :period_begin, :period_end,
     :execution_phase_string, :funding_source_string, :attachment_url,
     :funds_attachment_url]
  end

  def permits
    [:institution_id, :name, :allocation_of_resources, :resources_amount,
     :period_begin, :period_end, :funds_attachment, :enabled,
     :institution_dependency_id, :attachment, :execution_phase, :funding_source]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
