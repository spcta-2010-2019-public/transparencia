# frozen_string_literal: true
#
class RecruitmentResultsController < ApplicationController
  include Componentable

  def model
    InstitutionRecruitmentResult
  end

  def index
    super
    @disclaimers = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q] = {}
    params[:q][:name_cont] = params[:recruitment_name] unless params[:recruitment_name].blank?
    @q = @items.enabled.search(params[:q])
    @results = @q.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 20)
  end

  def filters
    super
    @document_categories = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_recruitment_results], @institution.id).order("name ASC")
    @documents = Document.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_recruitment_results], @institution.id).order('updated_at desc')
    @years = @documents.reorder("year ASC").map{|y| y.year}.uniq.delete_if{|y| y.to_i==0}
  end

  def breadcrumb
    super
    add_breadcrumb 'Resultado de selección y contratación de personal', institution_recruitment_results_url(@institution)
  end

end
