# frozen_string_literal: true
#
class HomeController < ApplicationController
  def index
    @institution_types = InstitutionType.enabled
    @institutions_count = Institution.enabled.count
    @services_count = Service.enabled.count
    @documents_count = Document.enabled.count
    total_registers = Consultant.where(enabled: true).count +
      Procurement.where(enabled: true).count +
      Official.where(enabled: true).count +
      Committee.count +
      InstitutionItem.where(enabled: true).count +
      ParticipationMechanism.where(enabled: true).count +
      ExecutingWork.where(enabled: true).count +
      ResourcesToPrivateRecipient.where(enabled: true).count +
      Remuneration.where(enabled: true).count +
      InstitutionRecruitmentResult.where(enabled: true).count +
      Service.where(enabled: true).count +
      Travel.where(enabled: true).count
    @total_count = @documents_count + total_registers
    @documents_download = Document.enabled.sum(:downloads)
    @last_update = Document.enabled.select(:updated_at).reorder(updated_at: :desc).limit(1).first
    @featured_resolutions = Document.featured_resolutions
    render layout: false
  end

  def search
    @results = PgSearch.multisearch(params[:ft]).includes(:searchable).paginate(:page => params[:page], :per_page => 10)
  end
end
