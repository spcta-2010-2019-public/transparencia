# frozen_string_literal: true
#
class ErrorsController < ApplicationController
  #
  def show
    @institution_types  = InstitutionType.enabled
    @institutions_count = Institution.enabled.count
    @documents_count    = Document.enabled.count

    status_code = params[:code] || 500
    render status_code.to_s, status: status_code
  end
end
