# frozen_string_literal: true
#
class PanelController < ApplicationController
  include Pundit

  before_action :authenticate_admin!
  before_action :set_paper_trail_whodunnit
  after_filter  :set_csrf_cookie_for_ng
  #
  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  def user_for_paper_trail
    admin_signed_in? ? current_admin.id : nil
  end

  def pundit_user
    current_admin
  end

  helper_method :menus
  #
  def menus
    @menu ||= Menu.all
  end

  helper_method :show_menu?
  #
  def show_menu?(menu)
    return false if hide_restricted_menu?(menu)
    return false if hide_dependencies_menu?(menu)
    #return false if hide_restricted_institution_actions_menu?(menu)
    #return false if hide_minsal_menu?(menu)
    # return false if hide_not_working_menu?(menu)
    menu.roles.include?(current_admin.role)
  end

  def hide_restricted_menu?(menu)
    current_admin.restricted_standards? &&
      menu.information_standard_id.positive? &&
      !current_admin.restricted_standard_ids.include?(
        menu.information_standard_id
      )
  end

  def hide_not_working_menu?(menu)
    current_admin.working_standards? &&
      menu.information_standard_id.positive? &&
      !current_admin.working_standard_ids.include?(
        menu.information_standard_id
      )
  end

  def hide_dependencies_menu?(menu)
    menu.controller == 'institution_dependencies' &&
      !current_admin.manage_dependencies?
  end

  def hide_restricted_institution_actions_menu?(menu)
    current_admin.restricted_standards? && current_admin.role == "oir" &&
    (menu.controller == 'oir_addresses' || menu.controller == 'institutions' ||
      menu.controller == 'statistics' || menu.controller == 'recycler_docks')
  end

  def hide_minsal_menu?(menu)
    minsal_id = Institution.where(acronym: "MINSAL").first.id rescue 20
    current_admin.restricted_standards? && current_admin.role == "oir" &&
    current_admin.institution_ids.include?(minsal_id) &&
    (menu.controller == 'documents' || menu.controller == 'document_categories' ||
    menu.controller == 'disclaimers')
  end

  protected

  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end
end
