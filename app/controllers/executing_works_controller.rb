# frozen_string_literal: true
#
class ExecutingWorksController < ApplicationController
  include PdfExportable
  include Componentable
  include CsvExportablePublic

  def model
    ExecutingWork
  end

  def index
    super
    @items_years                                                                               = @items
    @disclaimers                                                                               = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                                                                                 = {}
    params[:q][:executing_company_or_financing_source_or_location_or_name_or_responsible_cont] = params[:ew_executing_company_or_financing_source_or_location_or_name_or_responsible] unless params[:ew_executing_company_or_financing_source_or_location_or_name_or_responsible].blank?
    params[:q][:institution_dependency_id_eq]                                                  = params[:ew_institution_dependency_id]                                                unless params[:ew_institution_dependency_id].blank?
    @show_form                                                                                 = @items.count > 0
    @items                                                                                     = @items.where("starting_date BETWEEN ? AND ?", Date.new(params[:ew_date].to_i), Date.new(params[:ew_date].to_i).end_of_year) unless params[:ew_date].blank? rescue nil
    @fixed_params                                                                              = search_params(params)
    @ex_years                                                                                  = @items_years.where('starting_date IS NOT NULL').pluck("DISTINCT CAST(EXTRACT(year from starting_date) AS INT)").sort
    @q                                                                                         = @items.enabled.search(params[:q])
    @results                                                                                   = @q.result.order("year DESC, name ASC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:executing_works])
    @executing_work       = ExecutingWork.find(params[:id]) if action_name == "show" rescue nil
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:executing_works], @institution.id).order("name ASC")

    if @information_standard.priority_order?
      @documents         =  Document.enabled.where(:institution_id => @institution.id, :information_standard_id => Config[:dynamical_information_standards][:executing_works]).reorder("year DESC, priority DESC")
    else
      @documents         =  Document.enabled.where(:institution_id => @institution.id, :information_standard_id => Config[:dynamical_information_standards][:executing_works]).order('updated_at desc')
    end

    @years                = @documents.where('year > 0').reorder("year ASC").pluck('distinct(year)').sort
  end

  def breadcrumb
    super
    add_breadcrumb 'Obras en ejecución', institution_executing_works_url(@institution)
    add_breadcrumb truncate(@executing_work.name, :length => 50), institution_executing_work_url(@institution, @executing_work) rescue nil
  end

  def exportable_fields
    [:institution_id, :institution_dependency_id, :year, :name, :location,
     :starting_date, :beneficiaries_number, :amount, :execution_time,
     :responsible, :financing_source, :executing_company, :supervising_company,
     :contract_code, :guarantee, :payment_method]
  end

  def permits
    [:institution_id, :active, :enabled, :marked, :starting_date, :amount,
     :beneficiaries_number, :year, :executing_company, :execution_time,
     :financing_source, :location, :name, :responsible, :supervising_company,
     :contract_code, :guarantee, :payment_method, :institution_dependency_id]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
