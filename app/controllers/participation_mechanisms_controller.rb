# frozen_string_literal: true
#
class ParticipationMechanismsController < ApplicationController
  include PdfExportable
  include Componentable

  def model
    ParticipationMechanism
  end

  def index
    super
    @disclaimers                                                            = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                                                              = {}
    params[:q][:description_or_participation_requirements_or_objetive_cont] = params[:pm_fields] unless params[:pm_fields].blank?
    @items                                                                  = @items.where("execution_date_begin BETWEEN ? AND ?", Date.new(params[:pm_date].to_i), Date.new(params[:pm_date].to_i).end_of_year) unless params[:pm_date].blank? rescue nil
    @show_form                                                              = @items.count > 0
    @pm_years                                                               = @items.reorder("execution_date_begin ASC").map{|y| y.execution_date_begin.try(:year) }.compact.uniq.delete_if{|y| y.to_i==0}
    @q                                                                      = @items.enabled.search(params[:q])
    @results                                                                = @q.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 20)
  end


  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:participation_mechanisms])
    @mechanism            = ParticipationMechanism.find(params[:id]) if action_name == 'show'
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:participation_mechanisms], @institution.id).order("name ASC")

    if @information_standard.priority_order?
      @documents         =  Document.enabled.where(:institution_id => @institution.id, :information_standard_id => Config[:dynamical_information_standards][:participation_mechanisms]).reorder("year DESC, priority DESC")
    else
      @documents         =  Document.enabled.where(:institution_id => @institution.id, :information_standard_id => Config[:dynamical_information_standards][:participation_mechanisms]).order('updated_at desc')
    end

    @years               = @documents.where('year > 0').reorder('year ASC').pluck('DISTINCT CAST(year AS INT)').sort
  rescue
    nil
  end

  def breadcrumb
    super
    add_breadcrumb 'Mecanismos de participación ciudadana', institution_participation_mechanisms_url(@institution)
    add_breadcrumb truncate(@mechanism.name, length: 50),
                   institution_service_url(@institution, @mechanism)
  rescue
    nil
  end

  def exportable_fields
    [:institution_id, :institution_dependency_id, :name, :description,
     :participation_requirements, :objetive, :result]
  end
end
