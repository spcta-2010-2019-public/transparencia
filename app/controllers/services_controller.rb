# frozen_string_literal: true
#
class ServicesController < ApplicationController
  include PdfExportable
  include Componentable
  include CsvExportablePublic

  def model
    Service
  end

  def index
    super
    @disclaimers                                                                                  = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                                                                                    = {}
    params[:q][:name_or_responsible_area_or_responsible_name_or_description_or_requirements_cont] = params[:services_fields]                    unless params[:services_fields].blank?
    params[:q][:institution_dependency_id_eq]                                                     = params[:services_institution_dependency_id] unless params[:services_institution_dependency_id].blank?
    params[:q][:services_category_id_eq]                                                          = params[:services_category_id]               unless params[:services_category_id].blank?
    @services_categories                                                                          = ServicesCategory.where(institution_id: @institution.id).order("name ASC")
    @q                                                                                            = @items.enabled.search(params[:q])
    @fixed_params                                                                                 = search_params(params)
    @show_form                                                                                    = @items.count > 0
    @results                                                                                      = @q.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:institution_services])
    @service = Service.find(params[:id]) if action_name == "show" rescue nil
    @document_categories = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_services], @institution.id).order("name ASC")
    @documents = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_services], @institution.id).order('updated_at desc')
    @years = @documents.reorder("year ASC").map{|y| y.year}.uniq.delete_if{|y| y.to_i==0}
  end

  def breadcrumb
    super
    add_breadcrumb 'Servicios',
                   institution_services_url(@institution)
    add_breadcrumb @service.name,
                   institution_service_url(@institution, @service)
  rescue
    nil
  end

  def exportable_fields
    [:institution_id, :institution_dependency_id, :services_category_id, :name,
     :address, :schedule, :response_time, :responsible_area, :responsible_name,
     :description, :requirements, :cost, :observations]
  end


  def permits
    [:institution_id, :institution_dependency_id, :active, :enabled, :marked,
     :name, :response_time, :responsible_area, :responsible_name, :description,
     :requirements, :observations, :address, :schedule, :services_category_id,
     :cost]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
