# frozen_string_literal: true
module CsvExportablePublic
  extend ActiveSupport::Concern

  def csv_meta_data items
    [
      items.to_csv(exportable_fields),
      filename: csv_filename,
      type: 'text/csv; charset=iso-8859-1; header=present'
    ]
  end

  def csv_filename
    t(model.to_s.underscore, scope: 'activerecord.models', count: :many) +
      ' ' + l(Time.zone.now, format: :timestamp) +
      '.csv'
  end

  private
    def search_params fixed
      p = fixed.except(:controller, :action, :button, :q, :utf8)
    end
end
