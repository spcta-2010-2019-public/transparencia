module Componentable
  extend ActiveSupport::Concern

  included do
    respond_to :html, :json, :pdf
    before_filter :filters, :breadcrumb, :only => [:index, :show]
  end

  def model
    raise NoTypeGivenError
  end

  def index
    unless @institution.enabled?
      redirect_to category_url(@institution.institution_type) and return
    end

    @items = model.where(institution_id: @institution.id)

    params[:doc] = {}
    if params[:documents_year]
      params[:doc][:year_cont] = params[:documents_year] unless params[:documents_year].blank?
    end
    params[:doc][:name_or_description_cont] = params[:documents_name_or_description]    unless params[:documents_name_or_description].blank?
    params[:doc][:document_category_id_eq]  = params[:documents_document_category].to_i unless params[:documents_document_category].blank?

    @docs       = @documents.search(params[:doc])
    @doc_items  = @docs.result.order("updated_at DESC").paginate(page: params[:document_page], :per_page => 30)

  end

  def show
    unless @institution.enabled?
      redirect_to category_url(@institution.institution_type) and return
    end

    respond_to do |format|
      format.html
      format.pdf { super }
    end
  end

  def item_for_pdf
    model.find params[:id]
  end

  # rubocop:disable MethodLength
  def dynamic_standars
    {
      executing_works: 'executing_works',
      privates: 'resources_to_private_recipients',
      contracts: 'institution_procurements',
      services: 'institution_services',
      officials: 'institution_officials',
      consultants: 'institution_consultants',
      remunerations: 'institution_remunerations',
      inventories: 'institution_inventory_categories',
      travels: 'institution_travels',
      participation_mechanisms: 'participation_mechanisms',
      selections: 'institution_recruitment_results'
    }
  end

  def dynamic_standar
    dynamic_standars[controller_name.to_sym]
  end

  def filters
    @institution = Institution.friendly.find(params[:institution_id])

    unless dynamic_standar.nil?
      s = InformationStandard.find(Config[:dynamical_information_standards][dynamic_standar])
      i = @institution.shared_institution_standard(s) unless s.nil?
      @institution = i unless i.nil?
    end

    @institution_standards = @institution.standard_category.information_standards.order(:information_standard_frame_id, :name).group_by { |t| t.information_standard_frame_id }
    @dependencies          = @institution.institution_dependencies
    @category              = InstitutionType.enabled.find(@institution.institution_type.id)
  end

  def breadcrumb
    add_breadcrumb 'Inicio', root_url
    add_breadcrumb @category.name, category_url(@category)
    add_breadcrumb @institution.name, institution_url(@institution)
  end
end
