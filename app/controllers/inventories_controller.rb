# frozen_string_literal: true
#
class InventoriesController < ApplicationController
  include PdfExportable
  include Componentable
  include CsvExportablePublic

  def model
    InstitutionItem
  end

  def index
    super
    @disclaimers           = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]             = {}
    params[:q][:name_cont] = params[:inv_name] unless params[:inv_name].blank?
    params[:q][:year_eq]   = params[:inv_year] unless params[:inv_year].blank?
    @years_ii              = InstitutionItem.enabled.where(institution_id: @institution.id).where('year > 0').pluck('DISTINCT CAST(year AS INT)').sort
    @items                 = model.enabled.where(institution_id: @institution.id)
    @fixed_params          = search_params(params)
    @show_form             = @items.count > 0
    @q                     = @items.search(params[:q])
    @results               = @q.result.order("updated_at DESC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:institution_inventory_categories])
    @item                 = InstitutionItem.find(params[:id]) if action_name == "show"
    @document_categories  = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_inventory_categories], @institution.id).order("name ASC")

    if @information_standard.priority_order?
      @documents = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_inventory_categories], @institution.id).reorder("year DESC, priority DESC")
    else
      @documents = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_inventory_categories], @institution.id).order('updated_at desc')
    end

    @years                = @documents.where('year > 0').reorder("year ASC").pluck('DISTINCT year').sort
  end

  def breadcrumb
    super
    add_breadcrumb 'Inventarios', institution_inventories_url(@institution)
  end

  def exportable_fields
    [:institution_id, :year, :name, :brand, :description, :market_value,
     :current_value, :acquisition_date, :file_url]
  end

  def permits
    [:enabled, :name, :description, :brand, :market_value, :current_value,
     :acquisition_date, :file, :institution_id, :year]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
