module Api
  module V1
    class ConsultantsController < TemplateInstitutionParamsController
      def model
        Consultant
      end
    end
  end
end
