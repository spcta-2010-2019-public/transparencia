module Api
  module V1
    class OfficialsController < TemplateInstitutionParamsController
      def model
        Official
      end
    end
  end
end
