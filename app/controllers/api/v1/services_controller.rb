module Api
  module V1
    class ServicesController < TemplateInstitutionParamsController
      def model
        Service
      end
    end
  end
end
