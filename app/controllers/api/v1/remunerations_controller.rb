module Api
  module V1
    class RemunerationsController < TemplateInstitutionParamsController
      def model
        Remuneration
      end
    end
  end
end
