module Api
  module V1
    class DependenciesController < TemplateController
      def model
        InstitutionDependency
      end

      def controller_route
        "api/v1/dependencies"
      end
      helper_method :controller_route


    end
  end



end
