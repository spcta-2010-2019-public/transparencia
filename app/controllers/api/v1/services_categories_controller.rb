module Api
  module V1
    class ServicesCategoriesController < TemplateInstitutionParamsController
      def model
        ServicesCategory
      end
    end
  end
end
