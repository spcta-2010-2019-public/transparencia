class Api::V1::FullTemplateController < ApplicationController
  def index
    add_breadcrumb 'Inicio', root_url
    add_breadcrumb 'API', api_v1_home_index_url
    add_breadcrumb model.name.to_s
    respond_to do |format|
      format.html
      format.json {
        page = params[:page].to_i > 0 ? params[:page].to_i : 1
        per_page = params[:per_page].to_i > 0 ? params[:per_page].to_i : 100

        records = model.reorder(:id)
        records = records.where('created_at > ?', Date.parse(params[:start_at]).beginning_of_day) if params[:start_at] rescue nil
        records = records.where('created_at < ?', Date.parse(params[:end_at]).end_of_day) if params[:end_at] rescue nil
        records = records.where('institution_id = ?', params[:institution_id]) if params[:institution_id] rescue nil
        records = records.where('information_standard_id = ?', params[:information_standard_id]) if params[:information_standard_id] rescue nil
        records = records.paginate(page: page, per_page: per_page)
        if model.respond_to?(:external_json_methods)
          render json: records.as_json(methods: model::external_json_methods)
        else
          render json: records
        end
      }
    end
  end


  def model
  end
  helper_method :model

  def controller_route
    "api/v1/#{model.name.tableize}"
  end
  helper_method :controller_route
end
