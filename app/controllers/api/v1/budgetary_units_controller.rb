module Api
  module V1
    class BudgetaryUnitsController < TemplateInstitutionParamsController
      def model
        BudgetaryUnit
      end
    end
  end
end
