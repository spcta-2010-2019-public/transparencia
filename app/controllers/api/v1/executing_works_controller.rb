module Api
  module V1
    class ExecutingWorksController < TemplateInstitutionParamsController
      def model
        ExecutingWork
      end
    end
  end
end
