module Api
  module V1
    class StandardCategoriesController < TemplateController
      def model
        StandardCategory
      end
    end
  end
end
