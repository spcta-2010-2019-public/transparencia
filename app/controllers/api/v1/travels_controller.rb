module Api
  module V1
    class TravelsController < TemplateInstitutionParamsController
      def model
        Travel
      end
    end
  end
end
