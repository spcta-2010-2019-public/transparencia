module Api
  module V1
    class ParticipationMechanismsController < TemplateInstitutionParamsController
      def model
        ParticipationMechanism
      end
    end
  end
end
