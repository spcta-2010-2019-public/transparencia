module Api
  module V1
    class InstitutionTypesController < TemplateController
      def model
        InstitutionType
      end
    end
  end
end
