module Api
  module V1
    class InventoriesController < TemplateInstitutionParamsController
      def model
        InstitutionItem
      end

      def controller_route
        "api/v1/inventories"
      end
      helper_method :controller_route

    end
  end
end
