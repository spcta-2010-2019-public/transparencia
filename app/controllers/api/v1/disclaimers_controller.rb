module Api
  module V1
    class DisclaimersController < FullTemplateController
      def model
        Disclaimer
      end
    end
  end
end
