module Api
  module V1
    class DocumentsController < FullTemplateController
      def model
        Document
      end
    end
  end
end
