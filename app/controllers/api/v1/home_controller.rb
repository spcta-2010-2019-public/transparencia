class Api::V1::HomeController < ApplicationController
  def index
    add_breadcrumb 'Inicio', root_url
    add_breadcrumb 'API'
  end
end
