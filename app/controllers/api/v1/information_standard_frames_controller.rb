module Api
  module V1
    class InformationStandardFramesController < TemplateController
      def model
        InformationStandardFrame
      end
    end
  end
end
