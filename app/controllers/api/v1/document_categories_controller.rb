module Api
  module V1
    class DocumentCategoriesController < FullTemplateController
      def model
        DocumentCategory
      end
    end
  end
end
