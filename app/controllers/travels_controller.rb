# frozen_string_literal: true
#
class TravelsController < ApplicationController
  include PdfExportable
  include Componentable
  include CsvExportablePublic

  def model
    Travel
  end

  def index
    super
    @disclaimers                                                              = Disclaimer.enabled.where(institution_id: @institution.id, information_standard_id: @information_standard.id)
    params[:q]                                                                = {}
    params[:q][:name_or_officer_name_or_officer_position_or_destination_cont] = params[:remunerations_fields] unless params[:remunerations_fields].blank?
    params[:q][:institution_dependency_id_eq]                                 = params[:travels_institution_dependency_id] unless params[:travels_institution_dependency_id].blank?
    @travels_years                                                            = @items.reorder("departure_date ASC").map{|y| y.departure_date.year}.uniq.delete_if{|y| y.to_i==0}
    @items                                                                    = @items.where("departure_date BETWEEN ? AND ?", Date.new(params[:travels_date].to_i), Date.new(params[:travels_date].to_i).end_of_year) unless params[:travels_date].blank? rescue nil
    @fixed_params                                                             = search_params(params)
    @show_form                                                                = @items.count > 0
    @q                                                                        = @items.enabled.search(params[:q])
    @results                                                                  = @q.result.reorder("departure_date DESC").paginate(page: params[:page], :per_page => 20)

    respond_to do |format|
      format.html
      format.csv  { respond_to_csv }
    end
  end

  def filters
    super
    @information_standard = InformationStandard.find(Config[:dynamical_information_standards][:institution_travels])
    @travel = Travel.find(params[:id]) if action_name == 'show'
    @document_categories = DocumentCategory.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_travels], @institution.id).order("name ASC")

    if @information_standard.priority_order?
      @documents = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_travels], @institution.id).reorder("year DESC, priority DESC")
    else
      @documents = Document.enabled.where("information_standard_id = ? AND institution_id = ?", Config[:dynamical_information_standards][:institution_travels], @institution.id).order('updated_at desc')
    end

    @years = @documents.where('year > 0').reorder("year ASC").pluck('DISTINCT year').sort
  rescue
    nil
  end

  def breadcrumb
    super
    add_breadcrumb 'Viajes', institution_travels_url(@institution)
    add_breadcrumb truncate(@travel.name, length: 50),
                   institution_service_url(@institution, @travel)
  rescue
    nil
  end

  def exportable_fields
    [:institution_id, :institution_dependency_id, :name,
     :officer_name, :officer_position, :destination, :departure_date,
     :return_date, :passage_amount, :passage_payer, :passage_payer_comments,
     :viatical_amount, :viatical_payer, :viatical_payer_comments,
     :expenses_amount, :expenses_payer, :expenses_payer_comments,
     :objective, :observations]
  end

  def permits
    [:institution_id, :institution_dependency_id, :enabled, :marked, :name,
     :officer_name, :officer_position, :destination, :passage_amount,
     :viatical_amount, :expenses_amount, :departure_date, :return_date,
     :objective, :observations, :passage_payer, :viatical_payer,
     :expenses_payer, :passage_payer_comments, :viatical_payer_comments,
     :expenses_payer_comments]
  end

  def respond_to_csv
    unless params[:q].nil?
      @q       = @items.search(params[:q])
      @results = @q.result.order("updated_at DESC")
    end

    send_data(*csv_meta_data(@results))
  end
end
