# frozen_string_literal: true
module Panel
  #
  class StatisticsController < PanelController
    before_action :breacrumb

    def index

      @institutions = policy_scope(Institution).order(:acronym)
      @access_token = GoogleServiceAccount.access_token
      @information_standards = {}

      policy_scope(InformationStandard)
        .where.not(id: [51]) #ESTANDAR TEMPORAL PARA ELECCION DE COMISIONADOS IAIP
        .select(:id, :name)
        .each { |i| @information_standards[i.id] = i.name }
    end

    def breacrumb
      add_breadcrumb t('home'), panel_root_url
      add_breadcrumb t('activerecord.models.statistic', count: :many)
    end

    helper_method :institutions
    #
    def institutions
      return nil if current_admin.admin?
      current_admin.institutions.pluck(:id)
    rescue
      nil
    end

    def standards_downloaded
      @info = standards_downloaded_collection
              .limit(10)
              .map { |s| [s.name, s.downloads] }

      render json: @info
    end

    def documents_downloaded
      @info = documents_downloaded_collection
              .limit(10)
              .map { |s| [s.name, s.downloads] }

      render json: @info
    end

    private

    def standards_downloaded_collection
      if params[:ids].present?
        InformationStandard.top_institution_downloaded(params[:ids])
      elsif current_admin.admin?
        InformationStandard.top_downloaded
      else
        InformationStandard.top_institution_downloaded(policy_scope(Institution)
                                                      .pluck(:id))
      end
    end

    def documents_downloaded_collection
      if params[:ids].present?
        Document.top_institution_downloaded(params[:ids])
      elsif current_admin.admin?
        Document.top_downloaded
      else
        Document.top_institution_downloaded(policy_scope(Institution)
                                                      .pluck(:id))
      end
    end
  end
end
