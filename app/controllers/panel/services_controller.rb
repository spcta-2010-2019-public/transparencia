# frozen_string_literal: true
module Panel
  #
  class ServicesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      Service
    end

    def permits
      [:institution_id, :institution_dependency_id, :services_category_id,
       :active, :enabled, :marked, :name, :address, :schedule, :response_time,
       :responsible_area, :responsible_name, :description, :requirements,
       :observations, :cost,
       service_attachments_attributes: [:id, :name, :description, :attachment,
                                        :_destroy]]
    end

    def exportable_fields
      [:institution_id, :institution_dependency_id, :services_category_id,
       :active, :enabled, :name, :address, :schedule, :response_time,
       :cost, :responsible_area, :responsible_name, :description, :requirements,
       :observations]
    end

    def init_form
      @institutions             = policy_scope(Institution)
                                  .order(Institution.acts_as_label)

      @institution_dependencies = policy_scope(InstitutionDependency)
                                  .order(InstitutionDependency.acts_as_label)

      @services_categories      = policy_scope(ServicesCategory)
                                  .order(ServicesCategory.acts_as_label)
    end
  end
end
