# frozen_string_literal: true
module Panel
  #
  class ProcurementsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      Procurement
    end

    def permits
      [:institution_id, :year, :priority, :name, :amount, :form_of_contract,
       :counterpart_info, :compliance_deadlines, :attachment, :start_date,
       :counterpart_name, :adquisition_code, :institutional_area,
       :contract_code, :enabled]
    end

    def exportable_fields
      [:institution_id, :year, :priority, :name, :amount, :form_of_contract,
       :counterpart_info, :compliance_deadlines, :start_date, :counterpart_name,
       :adquisition_code, :institutional_area, :contract_code]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
    end
  end
end
