# frozen_string_literal: true
module Panel
  #
  class ViaticalPayersController < PanelController
    def list
      l = Travel.viatical_payers.keys.map do |i|
        {
          id: i,
          name: I18n.t(i, scope: 'activerecord.enum.travel.viatical_payer')
        }
      end

      render json: l
    end
  end
end
