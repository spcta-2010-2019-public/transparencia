# frozen_string_literal: true
module Panel
  #
  class DisclaimersController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      Disclaimer
    end

    def permits
      [:institution_id, :institution_dependency_id, :enabled,
       :name, :description, :information_standard_id, :file]
    end

    def exportable_fields
      [:institution_id, :institution_dependency_id, :enabled,
       :name, :description, :information_standard_id, :created_at, :updated_at]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      @institution_dependencies = policy_scope(InstitutionDependency)
                                  .order(Institution.acts_as_label)
      @information_standards    = policy_scope(InformationStandard)
                                  .order(InformationStandard.acts_as_label)
    end
  end
end
