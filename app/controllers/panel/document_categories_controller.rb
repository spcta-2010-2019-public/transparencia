# frozen_string_literal: true
module Panel
  #
  class DocumentCategoriesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      DocumentCategory
    end

    # Override
    def list
      render json: policy_scope(model)
        .includes(:information_standard)
        .reorder('information_standards.name, document_categories.name')
        .map { |i| { id: i.id, name: i.acts_as_label } }
        .to_json
    end

    def permits
      [:institution_id, :information_standard_id, :priority, :name]
    end

    def exportable_fields
      [:institution_id, :information_standard_id, :priority, :name]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      @information_standards = policy_scope(InformationStandard)
                               .order(InformationStandard.acts_as_label)
    end
  end
end
