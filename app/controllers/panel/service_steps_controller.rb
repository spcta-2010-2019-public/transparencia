# frozen_string_literal: true
module Panel
  #
  class ServiceStepsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      ServiceStep
    end

    def permits
      [:service_id, :correlative, :name, :description, :responsible_name,
       :duration, :response_time, :schedule, :requirements,
       :documents_to_present,
       service_step_attachments_attributes: [:id, :name, :description,
                                             :attachment, :_destroy]]
    end

    def exportable_fields
      [:service_id, :correlative, :name, :description, :responsible_name,
       :duration, :response_time, :schedule, :requirements,
       :documents_to_present]
    end

    def init_form
      @services = policy_scope(Service).order(Service.acts_as_label)
    end
  end
end
