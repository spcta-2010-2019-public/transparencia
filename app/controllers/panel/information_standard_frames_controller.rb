# frozen_string_literal: true
module Panel
  #
  class InformationStandardFramesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable

    def model
      InformationStandardFrame
    end

    def permits
      [:priority, :name]
    end

    def exportable_fields
      [:priority, :name]
    end
  end
end
