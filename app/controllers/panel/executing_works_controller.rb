# frozen_string_literal: true
module Panel
  #
  class ExecutingWorksController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      ExecutingWork
    end

    def permits
      [:institution_id, :institution_dependency_id, :active, :enabled, :marked,
       :starting_date, :amount, :beneficiaries_number, :year,
       :executing_company, :execution_time, :financing_source, :location, :name,
       :responsible, :supervising_company, :contract_code, :guarantee,
       :payment_method]
    end

    def exportable_fields
      [:institution_id, :institution_dependency_id, :active, :enabled,
       :starting_date, :amount, :beneficiaries_number, :year,
       :executing_company, :execution_time, :financing_source, :location, :name,
       :responsible, :supervising_company, :contract_code, :guarantee,
       :payment_method, :created_at, :updated_at]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      @institution_dependencies = policy_scope(InstitutionDependency)
                                  .order(Institution.acts_as_label)
    end
  end
end
