# frozen_string_literal: true
module Panel
  #
  class InstitutionInventoryCategoriesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      InstitutionInventoryCategory
    end

    def permits
      [:year, :institution_id, :name, :enabled]
    end

    def exportable_fields
      [:year, :institution_id, :name, :enabled]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
    end

  end
end
