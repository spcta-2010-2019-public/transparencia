# frozen_string_literal: true
module Panel
  #
  class BudgetaryUnitsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      BudgetaryUnit
    end

    def permits
      [:institution_id, :digit, :name]
    end

    def exportable_fields
      [:institution_id, :digit, :name]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
    end
  end
end
