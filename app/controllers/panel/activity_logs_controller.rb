# frozen_string_literal: true
module Panel
  #
  class ActivityLogsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    include Panel::VersionUtilities

    def show
      @version = model.find params[:id]
      add_breadcrumb t('activerecord.models.activity_log', count: :many),
                     url_for(action: :index)
    end

    def institution_logs
      redirect_to panel_root_path unless current_admin.admin?
      @institutions = Institution.enabled
      @information_standards = InformationStandard.all.reorder("name ASC")
    end

    def model
      ActivityLog
    end

    def exportable_fields
      [:item_type, :item_id, :whodunnit, :created_at]
    end
  end
end
