# frozen_string_literal: true
module Panel
  #
  class StandardCategoriesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable

    def model
      StandardCategory
    end

    def permits
      [
        :name,
        standard_category_infos_attributes: [
          :id, :information_standard_id, :linked_institution_id
        ]
      ]
    end

    def exportable_fields
      [:name]
    end

    def init_form
      @information_standards = InformationStandard
                               .order(InformationStandard.acts_as_label)

      @institutions = Institution.order(Institution.acts_as_label)

      @information_standards.each do |i|
        next if already_builded?(@item, i)
        @item.standard_category_infos.build(information_standard: i)
      end
    end

    def already_builded?(item, information_standard)
      item
        .standard_category_infos
        .map(&:information_standard_id)
        .include?(information_standard.id)
    end
  end
end
