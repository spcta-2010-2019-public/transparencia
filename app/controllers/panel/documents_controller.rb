# frozen_string_literal: true
module Panel
  #
  class DocumentsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    skip_before_action :verify_authenticity_token,
                       only: :information_standard_document_categories

    def model
      Document
    end

    def permits
      [:institution_id, :institution_dependency_id, :information_standard_id,
       :document_category_id, :enabled, :active, :marked, :priority, :year,
       :name, :description, :document]
    end

    def exportable_fields
      [:institution_id, :institution_dependency_id, :information_standard_id,
       :document_category_id, :enabled, :active, :priority, :year, :name,
       :description, :download_url, :created_at, :updated_at]
    end

    def json_methods
      [:with_document]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      @institution_dependencies = policy_scope(InstitutionDependency)
                                  .order(Institution.acts_as_label)
      @information_standards    = policy_scope(InformationStandard)
                                  .order(InformationStandard.acts_as_label)
      @document_categories      = policy_scope(DocumentCategory)
                                  .order(DocumentCategory.acts_as_label)
    end

    def information_standard_document_categories
      @document_categories = {}

      @docs_categories = DocumentCategory.order("name ASC")
      if !params[:institution_id].blank? && !params[:information_standard_id].blank?
        @docs_categories = @docs_categories.where(institution_id: params[:institution_id]).where(information_standard_id: params[:information_standard_id])
      elsif !params[:institution_id].blank? && params[:information_standard_id].blank?
        @docs_categories = @docs_categories.where(institution_id: params[:institution_id])
      elsif params[:institution_id].blank? && !params[:information_standard_id].blank?
        @docs_categories = @docs_categories.where(information_standard_id: params[:information_standard_id])
      end

      @docs_categories.each do |document_category|
        @document_categories[document_category.id] = document_category.name
      end
      respond_to do |format|
        format.json { render json: @document_categories, callback: params[:callback] }
      end
    end
  end
end
