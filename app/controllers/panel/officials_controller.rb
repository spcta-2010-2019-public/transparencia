# frozen_string_literal: true
module Panel
  #
  class OfficialsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      Official
    end

    def permits
      [:institution_id, :institution_dependency_id, :committee_id,
       :enabled, :active, :marked, :priority, :name, :position, :phone, :email,
       :address, :functions, :curriculum]
    end

    def exportable_fields
      [:institution_id, :institution_dependency_id, :committee_id,
       :enabled, :active, :priority, :name, :position, :functions, :phone,
       :email, :address, :curriculum, :created_at, :updated_at]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      @institution_dependencies = policy_scope(InstitutionDependency)
                                  .order(Institution.acts_as_label)
      @committees = policy_scope(Committee).order(Committee.acts_as_label)
    end
  end
end
