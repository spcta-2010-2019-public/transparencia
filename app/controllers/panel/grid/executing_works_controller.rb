# frozen_string_literal: true
module Panel
  module Grid
    #
    class ExecutingWorksController < GridController
      include Mtable

      def model
        ExecutingWork
      end

      def permits
        [:enabled, :active, :institution_id, :institution_dependency_id,
         :year, :name, :location, :amount, :financing_source, :starting_date,
         :execution_time, :beneficiaries_number, :executing_company,
         :supervising_company, :responsible, :payment_method, :guarantee,
         :contract_code]
      end
    end
  end
end
