# frozen_string_literal: true
module Panel
  module Grid
    #
    class TravelsController < GridController
      include Mtable

      def model
        Travel
      end

      def permits
        [:enabled, :institution_id, :institution_dependency_id, :name,
         :officer_name, :officer_position, :destination, :departure_date,
         :return_date,
         :passage_amount, :passage_payer, :passage_payer_comments,
         :viatical_amount, :viatical_payer, :viatical_payer_comments,
         :expenses_amount, :expenses_payer, :expenses_payer_comments,
         :objective, :observations]
      end
    end
  end
end
