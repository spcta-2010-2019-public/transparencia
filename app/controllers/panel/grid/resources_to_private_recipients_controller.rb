# frozen_string_literal: true
module Panel
  module Grid
    #
    class ResourcesToPrivateRecipientsController < GridController
      include Mtable

      def model
        ResourcesToPrivateRecipient
      end

      def permits
        [:enabled, :institution_id, :institution_dependency_id, :priority,
         :name, :allocation_of_resources, :execution_phase, :period_begin,
         :period_end, :resources_amount]
      end
    end
  end
end
