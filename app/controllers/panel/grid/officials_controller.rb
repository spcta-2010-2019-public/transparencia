# frozen_string_literal: true
module Panel
  module Grid
    #
    class OfficialsController < GridController
      include Mtable

      def model
        Official
      end

      def permits
        [:enabled, :active, :institution_id, :institution_dependency_id,
         :committee_id, :priority, :name, :position, :functions, :phone, :email,
         :address]
      end
    end
  end
end
