# frozen_string_literal: true
module Panel
  module Grid
    #
    class ConsultantsController < GridController
      include Mtable

      def model
        Consultant
      end

      def permits
        [:enabled, :active, :institution_id, :institution_dependency_id,
         :name, :description, :position, :assigned_unit, :phone, :email,
         :remuneration]
      end
    end
  end
end
