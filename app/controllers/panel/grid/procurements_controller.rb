# frozen_string_literal: true
module Panel
  module Grid
    #
    class ProcurementsController < GridController
      include Mtable

      def model
        Procurement
      end

      def permits
        [:enabled, :institution_id, :adquisition_code, :priority, :name, :year,
         :institutional_area, :amount, :counterpart_name, :counterpart_info,
         :compliance_deadlines, :form_of_contract, :start_date, :contract_code]
      end
    end
  end
end
