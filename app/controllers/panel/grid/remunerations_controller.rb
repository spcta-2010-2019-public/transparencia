# frozen_string_literal: true
module Panel
  module Grid
    #
    class RemunerationsController < GridController
      include Mtable

      def model
        Remuneration
      end

      def permits
        [:enabled, :active, :year, :institution_id, :work_line_id, :name,
         :employees_number, :ad_honorem, :monthly_remuneration,
         :expenses_remuneration, :salary_category]
      end
    end
  end
end
