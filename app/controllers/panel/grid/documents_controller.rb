# frozen_string_literal: true
module Panel
  module Grid
    #
    class DocumentsController < GridController
      include Mtable

      def model
        Document
      end

      def permits
        [:enabled, :active, :priority, :year, :institution_id,
         :institution_dependency_id, :information_standard_id,
         :document_category_id, :name, :description]
      end
    end
  end
end
