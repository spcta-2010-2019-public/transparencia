# frozen_string_literal: true
module Panel
  module Grid
    #
    class InstitutionItemsController < GridController
      include Mtable

      def model
        InstitutionItem
      end

      def permits
        [:enabled, :year, :name, :description, :brand, :market_value,
         :current_value, :acquisition_date]
      end
    end
  end
end
