# frozen_string_literal: true
module Panel
  module Grid
    #
    class ServicesController < GridController
      include Mtable

      def model
        Service
      end

      def permits
        [:enabled, :active, :institution_id, :institution_dependency_id,
         :services_category_id, :name, :description, :address, :schedule,
         :response_time, :cost, :responsible_area, :responsible_name,
         :requirements, :observations]
      end
    end
  end
end
