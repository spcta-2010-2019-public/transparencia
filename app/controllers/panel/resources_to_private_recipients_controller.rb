# frozen_string_literal: true
module Panel
  #
  class ResourcesToPrivateRecipientsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      ResourcesToPrivateRecipient
    end

    def permits
      [
        :institution_id, :institution_dependency_id, :priority, :name,
        :allocation_of_resources, :resources_amount, :period_begin,
        :period_end, :enabled, :attachment, :funds_attachment,
        :execution_phase, funding_source: []
      ]
    end

    def exportable_fields
      [
        :institution_id, :institution_dependency_id, :priority, :name,
        :allocation_of_resources, :resources_amount, :period_begin,
        :period_end, :enabled, :execution_phase, :funding_source
      ]
    end

    def init_form
      @institutions = policy_scope(Institution)
                       .order(Institution.acts_as_label)

      @institution_dependencies = policy_scope(InstitutionDependency)
                                  .order(InstitutionDependency.acts_as_label)
    end
  end
end
