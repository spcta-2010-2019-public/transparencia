# frozen_string_literal: true
module Panel
  #
  class AdminsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    before_action :check_admin
    #

    def model
      Admin
    end

    def permits
      [:name, :email, :role, :password, :password_confirmation,
       :manage_dependencies, institution_ids: [], restricted_standard_ids: []]
    end

    def exportable_fields
      [:name, :email, :role, :manage_dependencies]
    end

    def init_form
      @institutions = Institution.order(Institution.acts_as_label)
      @information_standards = InformationStandard
                               .order(Institution.acts_as_label)
    end

    def update
      @item.attributes = item_params
      m = @item.password.blank? ? :update_without_password : :update_attributes

      return redirect_to(edit_url, flash: { saved: true }) if
        @item.send(m, item_params)

      breadcrumbs_for(:edit)
      init_form
      render template: 'concerns/panel/form'
    end

    private

    def check_admin
      redirect_to panel_root_url && return unless current_admin.admin?
    end
  end
end
