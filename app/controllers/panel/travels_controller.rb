# frozen_string_literal: true
module Panel
  #
  class TravelsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      Travel
    end

    def permits
      [:institution_id, :institution_dependency_id, :enabled, :marked, :name,
       :officer_name, :officer_position, :destination, :passage_amount,
       :passage_amount, :passage_payer, :passage_payer_comments,
       :viatical_amount, :viatical_payer, :viatical_payer_comments,
       :expenses_amount, :expenses_payer, :expenses_payer_comments,
       :departure_date, :return_date, :objective, :observations]
    end

    def exportable_fields
      [:institution_id, :institution_dependency_id, :enabled, :name,
       :officer_name, :officer_position, :destination, :departure_date,
       :passage_amount, :passage_payer, :passage_payer_comments,
       :viatical_amount, :viatical_payer, :viatical_payer_comments,
       :expenses_amount, :expenses_payer, :expenses_payer_comments,
       :return_date, :objective, :observations, :created_at, :updated_at]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      @institution_dependencies = policy_scope(InstitutionDependency)
                                  .order(Institution.acts_as_label)
    end
  end
end
