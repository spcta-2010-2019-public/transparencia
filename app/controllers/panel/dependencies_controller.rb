# frozen_string_literal: true
module Panel
  #
  class InstitutionDependenciesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    #

    def model
      InstitutionDependency
    end

    def permits
      [:institution_id, :name]
    end
  end
end
