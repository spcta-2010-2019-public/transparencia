# frozen_string_literal: true
module Panel
  #
  class ParticipationMechanismsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      ParticipationMechanism
    end

    def permits
      [:institution_dependency_id, :description, :participation_requirements, :execution_date_begin, :execution_date_end, :objetive, :result, :report, :institution_id, :name, :enabled] 
    end

    def exportable_fields
      [:institution_dependency_id, :description, :participation_requirements, :execution_date_begin, :execution_date_end, :objetive, :result, :report, :institution_id, :name, :enabled] 
    end

    def init_form
      @institutions = policy_scope(Institution)
                         .order(Institution.acts_as_label)

      @institution_dependencies = policy_scope(InstitutionDependency)
                                    .order(InstitutionDependency.acts_as_label)
    end

  end
end
