# frozen_string_literal: true
module Panel
  #
  class InstitutionItemsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    include PdfExportable
    #

    def model
      InstitutionItem
    end

    def permits
      [:enabled, :institution_id, :year, :name, :description, :brand,
       :market_value, :current_value, :acquisition_date, :file]
    end

    def exportable_fields
      [:enabled, :institution_id, :year, :name, :description, :brand,
       :market_value, :current_value, :acquisition_date]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
    end
  end
end
