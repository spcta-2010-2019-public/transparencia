# frozen_string_literal: true
module Panel
  #
  class ExpensesPayersController < PanelController
    def list
      l = Travel.expenses_payers.keys.map do |i|
        {
          id: i,
          name: I18n.t(i, scope: 'activerecord.enum.travel.expenses_payer')
        }
      end

      render json: l
    end
  end
end
