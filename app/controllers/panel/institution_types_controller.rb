# frozen_string_literal: true
module Panel
  #
  class InstitutionTypesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable

    def model
      InstitutionType
    end

    def permits
      [:enabled, :priority, :name]
    end

    def exportable_fields
      [:enabled, :priority, :name]
    end
  end
end
