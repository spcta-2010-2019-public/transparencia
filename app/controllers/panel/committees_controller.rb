# frozen_string_literal: true
module Panel
  #
  class CommitteesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable

    def model
      Committee
    end

    def permits
      [:institution_id, :name]
    end

    def exportable_fields
      [:institution_id, :name]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
    end
  end
end
