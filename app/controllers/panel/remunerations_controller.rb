# frozen_string_literal: true
module Panel
  #
  class RemunerationsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      Remuneration
    end

    def permits
      [:institution_id, :work_line_id, :active, :enabled, :marked, :ad_honorem,
       :name, :employees_number, :monthly_remuneration, :expenses_remuneration,
       :salary_category, :year]
    end

    def exportable_fields
      [:institution_id, :budgetary_unit_id, :work_line_id, :active, :enabled,
       :year, :ad_honorem, :name, :employees_number, :monthly_remuneration,
       :expenses_remuneration, :salary_category]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      @budgetary_units = policy_scope(BudgetaryUnit).order(:digit)
    end

    def json_includes
      [:budgetary_unit]
    end
  end
end
