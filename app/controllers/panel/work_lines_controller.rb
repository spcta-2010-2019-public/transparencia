# frozen_string_literal: true
module Panel
  #
  class WorkLinesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      WorkLine
    end

    def permits
      [:budgetary_unit_id, :digit, :code, :name]
    end

    def exportable_fields
      [:budgetary_unit_id, :digit, :code, :name]
    end

    def init_form
      @budgetary_units = policy_scope(BudgetaryUnit)
                         .order(BudgetaryUnit.acts_as_label)
    end

    # Override
    def list
      render json: policy_scope(model)
        .includes(:budgetary_unit)
        .order('budgetary_units.name, work_lines.name')
        .map { |i| { id: i.id, name: i.acts_as_label } }
        .to_json
    end
  end
end
