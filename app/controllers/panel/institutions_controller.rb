# frozen_string_literal: true
module Panel
  #
  class InstitutionsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      Institution
    end

    def permits
      [:institution_type_id, :standard_category_id, :accepts_online_requests,
       :administrative_document_type, :certification_amount,
       :reproduction_amount, :external_transparency_site_url, :facebook_url,
       :facebook_username, :officer_email, :officer_name, :twitter_url,
       :twitter_username, :website_url, :officer_designation_date,
       :institutions, :name, :acronym, :avatar, :enabled,
       :administrative_document, :officer_designation, :score,
       :youtube_username, :youtube_url, :flickr_username, :flickr_url,
       :rating_report]
    end

    def exportable_fields
      [:name, :acronym, :enabled, :institution_type_id, :standard_category_id,
       :score, :accepts_online_requests, :officer_name, :officer_email,
       :officer_designation_date, :certification_amount, :reproduction_amount,
       :administrative_document_type, :external_transparency_site_url,
       :website_url, :facebook_username, :facebook_url, :twitter_username,
       :twitter_url, :youtube_username, :youtube_url, :flickr_username,
       :flickr_url]
    end

    def init_form
      @institution_types   = InstitutionType
                             .order(InstitutionType.acts_as_label)

      @standard_categories = StandardCategory
                             .order(StandardCategory.acts_as_label)
    end
  end
end
