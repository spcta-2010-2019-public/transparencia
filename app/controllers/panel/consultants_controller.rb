# frozen_string_literal: true
module Panel
  #
  class ConsultantsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      Consultant
    end

    def permits
      [:institution_id, :institution_dependency_id, :enabled, :active, :marked,
       :name, :description, :remuneration, :position, :assigned_unit, :phone,
       :email, :applied_studies, :work_experience, :functions]
    end

    def exportable_fields
      [:institution_id, :institution_dependency_id, :enabled, :active,
       :name, :description, :remuneration, :position, :assigned_unit, :phone,
       :email, :applied_studies, :work_experience, :functions, :created_at,
       :updated_at]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      @institution_dependencies = policy_scope(InstitutionDependency)
                                  .order(Institution.acts_as_label)
    end
  end
end
