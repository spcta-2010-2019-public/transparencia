# frozen_string_literal: true
module Panel
  #
  class PassagePayersController < PanelController
    def list
      l = Travel.passage_payers.keys.map do |i|
        {
          id: i,
          name: I18n.t(i, scope: 'activerecord.enum.travel.passage_payer')
        }
      end

      render json: l
    end
  end
end
