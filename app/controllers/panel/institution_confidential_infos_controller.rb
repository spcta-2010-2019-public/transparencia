# frozen_string_literal: true
module Panel
  #
  class InstitutionConfidentialInfosController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      InstitutionConfidentialInfo
    end

    def permits
      [ :declaration, :category, :admin_unit, :classification_date,
        :classification_type, :legal_foundation, :justification,
        :declassification_date, :file, :institution_id, :status,
        institution_historical_confidential_infos_attributes:
        [:id, :comment, :last_date, :_destroy]
      ]
    end

    def exportable_fields
      [ :declaration, :category, :admin_unit, :classification_date,
        :classification_type, :legal_foundation, :justification,
        :declassification_date, :file, :institution_id, :status
      ]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
    end
  end
end
