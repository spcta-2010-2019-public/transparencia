# frozen_string_literal: true
module Panel
  #
  class ExecutionPhasesController < PanelController
    def list
      scp = 'activerecord.enum.resources_to_private_recipient.execution_phase'
      l = ResourcesToPrivateRecipient.execution_phases.keys.map do |i|
        { id: i, name: I18n.t(i, scope: scp) }
      end

      render json: l
    end
  end
end
