# frozen_string_literal: true
module Panel
  #
  class OirAddressesController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      OirAddress
    end

    def permits
      [:institution_id, :enabled, :address, :phone, :lat, :lng,
       schedule: [:wday, :from, :to]]
    end

    def exportable_fields
      [:institution_id, :enabled, :address, :phone, :lat, :lng, :schedule]
    end

    def init_form
      @institutions = policy_scope(Institution).order(Institution.acts_as_label)
      init_schedule
      init_coords
    end

    def init_coords
      return if @item.lat.present? && @item.lng.present?

      @item.lat = OirAddress::DEFAULT_COORDS[:lat]
      @item.lng = OirAddress::DEFAULT_COORDS[:lng]
    end

    def init_schedule
      @item.schedule = Date::DAYNAMES.map do |wday|
        if @item.schedule.map { |s| s[:wday] }.include?(wday.downcase)
          @item.schedule.select { |s| s[:wday] == wday.downcase }.first
        else
          { wday: wday.downcase, from: nil, to: nil }
        end
      end.compact
    end
  end
end
