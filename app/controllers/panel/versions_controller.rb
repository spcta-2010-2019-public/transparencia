# frozen_string_literal: true
module Panel
  #
  class VersionsController < PanelController
    include Panel::VersionUtilities
  end
end
