# frozen_string_literal: true
module Panel
  #
  class InformationStandardsController < PanelController
    include Panel::Crud
    include Panel::Multiple
    include Panel::CsvExportable
    #

    def model
      InformationStandard
    end

    def permits
      [:information_standard_frame_id, :priority, :name, :short_name,
       :priority_order, :description]
    end

    def exportable_fields
      [:information_standard_frame_id, :priority, :name, :short_name,
       :description]
    end

    def init_form
      @information_standard_frames = InformationStandardFrame
                                     .order(
                                       InformationStandardFrame.acts_as_label
                                     )
    end
  end
end
