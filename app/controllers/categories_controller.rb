# frozen_string_literal: true
#
class CategoriesController < ApplicationController
  def show
    @category = InstitutionType.enabled.find params[:id]
    @breadcrumb = InstitutionType.find(@category)

    add_breadcrumb 'Inicio', root_url
    add_breadcrumb @breadcrumb.name, category_url(@breadcrumb)

    @institutions = Institution.enabled.i_type @category
    @institution_types = InstitutionType.enabled
  end

  helper_method :show_acronym?
  #
  def show_acronym?
    ![5].include? @category.id
  end
end
