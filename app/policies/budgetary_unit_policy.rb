# frozen_string_literal: true
#
class BudgetaryUnitPolicy < ApplicationPolicy
  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      return scope.all if user.admin?
      scope.where(oir_conditions)
    end

    def oir_conditions
      { id: user.budgetary_units.pluck(:id) }
    end
  end
end
