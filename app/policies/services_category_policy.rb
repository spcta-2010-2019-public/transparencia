# frozen_string_literal: true
#
class ServicesCategoryPolicy < ApplicationPolicy
  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.admin?
        scope.all
      else
        scope.where(id: user.services_categories.pluck(:id))
      end
    end
  end
end
