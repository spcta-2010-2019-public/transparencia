# frozen_string_literal: true
#
class DisclaimerPolicy < ApplicationPolicy
  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      return scope.all if user.admin?
      scope.where(oir_conditions)
    end

    def oir_conditions
      {
        id: user.disclaimers.pluck(:id),
        information_standard_id: information_standard_ids
      }
    end

    def information_standard_ids
      return user.restricted_standards.pluck(:id) if user.restricted_standards?
      user.information_standards.pluck(:id)
    end
  end
end
