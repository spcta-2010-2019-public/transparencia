# frozen_string_literal: true
#
class ServiceStepPolicy < ApplicationPolicy
  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      return scope.all if user.admin?
      scope.where(oir_conditions)
    end

    def oir_conditions
      { id: user.service_steps.pluck(:id) }
    end
  end
end
