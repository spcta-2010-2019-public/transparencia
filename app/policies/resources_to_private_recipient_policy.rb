# frozen_string_literal: true
#
class ResourcesToPrivateRecipientPolicy < ApplicationPolicy
  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.admin?
        scope.all
      else
        scope.where(id: user.resources_to_private_recipients.pluck(:id))
      end
    end
  end
end
