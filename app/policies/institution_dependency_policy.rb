# frozen_string_literal: true
#
class InstitutionDependencyPolicy < ApplicationPolicy
  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.admin?
        scope.all
      else
        scope.where(id: user.institution_dependencies.pluck(:id))
      end
    end
  end
end
