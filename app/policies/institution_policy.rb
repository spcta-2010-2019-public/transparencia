# frozen_string_literal: true
#
class InstitutionPolicy < ApplicationPolicy
  #
  def destroy?
    user.admin?
  end

  def create?
    user.admin?
  end

  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.admin?
        scope.all
      else
        scope.where(id: user.institution_ids)
      end
    end
  end
end
