# frozen_string_literal: true
#
class AdminPolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    user.admin?
  end

  def update?
    return true if user.admin?
    record.id == user.id
  end

  def edit?
    update?
  end
end
