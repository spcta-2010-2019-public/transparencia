# frozen_string_literal: true
#
class RecyclerDockPolicy < ApplicationPolicy
  #
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def create?
    false
  end

  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      return scope.all if user.admin?
      scope.where(whodunnit: user.partners.pluck(:id))
    end
  end
end
