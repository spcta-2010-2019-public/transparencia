# frozen_string_literal: true
#
class InstitutionItemPolicy < ApplicationPolicy
  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.admin?
        scope.all
      else
        scope.where(id: user.institution_items.pluck(:id))
      end
    end
  end
end
