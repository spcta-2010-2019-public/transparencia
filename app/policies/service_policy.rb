# frozen_string_literal: true
#
class ServicePolicy < ApplicationPolicy
  #
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      return scope.all if user.admin?
      scope.where(oir_conditions)
    end

    def oir_conditions
      c = { id: user.services.pluck(:id) }
      # if user.manage_dependencies?
      #   c[:institution_dependency_id] = user
      #                                   .institution_dependencies
      #                                   .pluck(:id)
      # end
      c
    end
  end
end
