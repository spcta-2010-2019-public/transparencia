# frozen_string_literal: true
#
class Official < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  belongs_to :institution_dependency
  belongs_to :committee

  # Validations
  validates :institution_id, presence: true
  validates :name,           presence: true
  validates :position,       presence: true
  validates :phone,          presence: true
  validates :functions,      presence: true
  validates :curriculum,     presence: true

  validates :email, presence: true
  validates :email,
            format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i },
            if: proc { |o| o.email.present? }

  validates :priority, presence: true
  validates :priority,
            numericality: { only_integer: true },
            unless: proc { |o| o.priority.blank? }

  # Scopes
  scope :enabled, -> { where(enabled: true) }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  ransacker :position, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"position\")")
  end

  def active_string
    return 'Vigente' if active?
    'No vigente'
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def institution_dependency_id=(string)
    return super if number?(string)
    id_from_string(InstitutionDependency, string)
  end

  def committee_id=(string)
    return super if number?(string)
    id_from_string(Committee, string)
  end

  def self.grid?
    true
  end
end
