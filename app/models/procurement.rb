# frozen_string_literal: true
#
class Procurement < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution

  # Validations
  validates :name,                  presence: true
  validates :institution_id,        presence: true
  validates :amount,                presence: true
  validates :counterpart_info,      presence: true
  validates :compliance_deadlines,  presence: true
  validates :amount, numericality: { greater_than_or_equal_to: 0.0 }

  validates :year, presence: true
  validates :year,
            numericality: { only_integer: true, greater_than: 0 },
            length: { is: 4 }

  validates :priority, presence: true
  validates :priority,
            numericality: { only_integer: true },
            unless: proc { |o| o.priority.blank? }

  scope :enabled, -> { where(enabled: true) }

  has_attached_file :attachment
  do_not_validate_attachment_file_type :attachment

  def attachment_url
    return '' unless attachment.present?
    URI.join(ActionController::Base.asset_host, attachment.url) rescue nil
  end

  serialize :form_of_contract

  ransacker :year, type: :string do
    Arel.sql('TO_CHAR("year", \'9999\')')
  end

  ransacker :start_date_year, type: :string do
    Arel.sql('DATE_PART(\'year\', "start_date")')
  end

  ransacker :amount, type: :string do
    Arel.sql('TO_CHAR("amount", \'999999999999D9999\')')
  end

  ransacker :form_of_contract, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"form_of_contract\")")
  end

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  ransacker :counterpart_name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"counterpart_name\")")
  end

  ransacker :counterpart_info, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"counterpart_info\")")
  end

  ransacker :compliance_deadlines, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"compliance_deadlines\")")
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def self.grid?
    true
  end

  def self.years
    where('year > 0')
      .reorder(:year)
      .pluck('DISTINCT("procurements"."year")')
      .sort
  end
end
