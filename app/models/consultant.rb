# frozen_string_literal: true
#
class Consultant < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  belongs_to :institution_dependency

  # Validations
  validates :institution_id,  presence: true
  validates :name,            presence: true
  validates :description,     presence: true
  validates :position,        presence: true
  validates :phone,           presence: true
  validates :assigned_unit,   presence: true
  validates :applied_studies, presence: true
  validates :work_experience, presence: true
  validates :functions,       presence: true

  validates :remuneration, presence: true
  validates :remuneration,
            numericality: { greater_than: 0.0 }

  validates :email, presence: true
  validates :email,
            format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i },
            if: proc { |o| o.email.present? }

  # Scopes
  default_scope { order('consultants.marked DESC, consultants.name DESC') }

  scope :enabled, -> { where(enabled: true) }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  ransacker :position, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"position\")")
  end

  ransacker :assigned_unit, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"assigned_unit\")")
  end

  def active_string
    return 'Vigente' if active?
    'No vigente'
  rescue
    nil
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def institution_dependency_id=(string)
    return super if number?(string)
    id_from_string(InstitutionDependency, string)
  end

  def self.grid?
    true
  end
end
