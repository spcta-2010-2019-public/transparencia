# frozen_string_literal: true
#
class InformationStandard < ApplicationRecord
  include CsvExportable
  include Versionable

  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  belongs_to :information_standard_frame
  has_many :standard_category_infos

  has_many :documents
  # has_and_belongs_to_many :institutions

  has_many :disclaimers

  validates :information_standard_frame_id, presence: true

  validates :priority, presence: true
  validates :priority, numericality: { only_integer: true }

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  validates :short_name, presence: true
  validates :short_name, uniqueness: { case_sensitive: false }

  default_scope { joins(:information_standard_frame).order('information_standard_frames.priority DESC').order('information_standards.name ASC') }

  scope :top_downloaded, lambda {
    select('information_standards.*, SUM(documents.downloads) AS downloads')
      .joins('LEFT JOIN documents ON
              (documents.information_standard_id = information_standards.id)')
      .where('downloads > 0')
      .group('information_standards.id')
      .reorder('downloads DESC')
  }

  scope :top_institution_downloaded, lambda { |institution_ids|
    top_downloaded.where(documents: { institution_id: institution_ids })
  }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  ransacker :short_name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"short_name\")")
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end
end
