# frozen_string_literal: true
# rubocop:disable ClassLength
class Institution < ApplicationRecord
  include CsvExportable
  include Versionable

  extend FriendlyId
  friendly_id :acronym, use: [:slugged, :finders]

  has_and_belongs_to_many :admins

  belongs_to :institution_type
  belongs_to :standard_category

  has_many :budgetary_units, dependent: :destroy
  has_many :committees, dependent: :destroy
  has_many :consultants, dependent: :destroy
  has_many :document_categories, dependent: :destroy
  has_many :documents, dependent: :destroy
  has_many :executing_works, dependent: :destroy
  has_many :institution_dependencies, dependent: :destroy
  has_many :officials, dependent: :destroy
  has_many :oir_addresses, dependent: :destroy
  has_many :services_categories, dependent: :destroy
  has_many :services, dependent: :destroy
  has_many :remunerations, dependent: :destroy
  has_many :travels, dependent: :destroy
  has_many :service_steps,     through: :services, dependent: :destroy
  has_many :work_lines,        through: :budgetary_units, dependent: :destroy
  # has_and_belongs_to_many :information_standards
  has_many :institution_inventory_categories, dependent: :destroy
  has_many :institution_items, dependent: :destroy
  has_many :institution_recruitment_results, dependent: :destroy
  has_many :institution_confidential_infos, dependent: :destroy
  has_many :resources_to_private_recipients, dependent: :destroy
  has_many :participation_mechanisms, dependent: :destroy
  has_many :procurements, dependent: :destroy
  has_many :disclaimers, dependent: :destroy

  enum administrative_document_type: [:administrative_act,
                                      :not_existence_declaration]

  # Validations
  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  validates :acronym, presence: true
  validates :acronym, uniqueness: { case_sensitive: false }

  validates :institution_type_id, presence: true
  validates :standard_category_id, presence: true

  validates :external_transparency_site_url,
            format: URI.regexp(%w(http https)),
            unless: proc { |o| o.external_transparency_site_url.blank? }

  # validates :score,
  #           numericality: { greater_than_or_equal_to: 0.0 }

  validates :reproduction_amount,
            numericality: { greater_than_or_equal_to: 0.0 }

  validates :certification_amount,
            numericality: { greater_than_or_equal_to: 0.0 }

  validates :administrative_document_type, presence: true
  validates :administrative_document, presence: true

  validates :website_url,
            format: URI.regexp(%w(http https)),
            unless: proc { |o| o.website_url.blank? }

  validates :facebook_url,
            presence: true,
            unless: proc { |o| o.facebook_username.blank? }

  validates :facebook_url,
            format: URI.regexp(%w(http https)),
            unless: proc { |o| o.facebook_url.blank? }

  validates :twitter_url,
            presence: true,
            unless: proc { |o| o.twitter_username.blank? }

  validates :twitter_url,
            format: URI.regexp(%w(http https)),
            unless: proc { |o| o.twitter_url.blank? }

  validates :youtube_url,
            presence: true,
            unless: proc { |o| o.youtube_username.blank? }

  validates :youtube_url,
            format: URI.regexp(%w(http https)),
            unless: proc { |o| o.youtube_url.blank? }

  validates :flickr_url,
            presence: true,
            unless: proc { |o| o.flickr_username.blank? }

  validates :flickr_url,
            format: URI.regexp(%w(http https)),
            unless: proc { |o| o.flickr_url.blank? }

  validates :officer_name, presence: true

  validates :officer_email, presence: true
  validates :officer_email,
            format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i },
            if: proc { |o| o.officer_email.present? }

  # validates :officer_designation_date, presence: true
  # validates :officer_designation, presence: true

  # Scopes
  default_scope { order(acts_as_label) }
  scope :i_type, ->(kind) { where('institution_type_id = ?', kind) }
  scope :enabled, -> { where(enabled: true) }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  has_attached_file :avatar, styles: { original: '960>', profile: '160x160>' }
  validates_attachment_content_type :avatar, content_type: /\Aimage/

  has_attached_file :administrative_document
  do_not_validate_attachment_file_type :administrative_document

  has_attached_file :officer_designation
  do_not_validate_attachment_file_type :officer_designation

  has_attached_file :rating_report
  do_not_validate_attachment_file_type :rating_report

  def full_name
    "#{acronym} - #{name}"
  end

  def shared_institution_standard(information_standard)
    StandardCategoryInfo
      .where(standard_category_id: standard_category.id)
      .where(information_standard_id: information_standard.id)
      .first
      .try(:linked_institution)
  end

  def documents_years_by_standard(standard)
    documents
      .where(information_standard_id: standard.id)
      .where('year > 0')
      .reorder(:year)
      .pluck('DISTINCT("documents"."year")')
      .sort
  end

  def avatar_file_url
    ActionController::Base.helpers.asset_url(avatar.url(:original, timestamp: false))
  end

  def administrative_document_file_url
    ActionController::Base.helpers.asset_url(administrative_document.url(:original, timestamp: false))
  end

  def officer_designation_file_url
    ActionController::Base.helpers.asset_url(officer_designation.url(:original, timestamp: false))
  end

  def rating_report_file_url
    ActionController::Base.helpers.asset_url(rating_report.url(:original, timestamp: false))
  end

  def self.external_json_methods
    %w(avatar_file_url administrative_document_file_url officer_designation_file_url rating_report_file_url)
  end
end
