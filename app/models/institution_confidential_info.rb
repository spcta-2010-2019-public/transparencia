class InstitutionConfidentialInfo < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  has_many :institution_historical_confidential_infos, dependent: :destroy
  accepts_nested_attributes_for :institution_historical_confidential_infos,
                                  allow_destroy: true

  validates :declaration, :category, :admin_unit, :classification_date,
            :classification_type, :legal_foundation, :justification,
            :declassification_date, :institution_id, presence: true

  after_create :verify_status, if: Proc.new{|o| o.status.blank?}

  STATUS = %w[created extension declassified]
  #Model::STATUS.map{ |kind| [t(".#{kind}"), kind]}

  default_scope { order(acts_as_label) }

  def self.acts_as_label
    :declaration
  end

  private
  def verify_status
    if self.status.blank?
      self.status = "created"
      self.save!
    end
  end


end
