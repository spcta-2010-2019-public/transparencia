# frozen_string_literal: true
#
class GoogleServiceAccount < ApplicationRecord
  #
  SCOPE       = 'https://www.googleapis.com/auth/analytics.readonly'
  CREDENTIALS = Rails.root.join('google-service-account.json').to_s

  def self.instance
    o = first_or_create(expires_in: Time.zone.now - 1.seconds)
    return o unless o.expired?
    o.refresh_access_token!
    o
  end

  column_names.each do |column_name|
    next if method_defined? column_name
    define_singleton_method column_name do
      instance.send(column_name)
    end
  end

  def fetch_access_token
    authorizer = Google::Auth::ServiceAccountCredentials.make_creds(
      json_key_io: File.open(CREDENTIALS),
      scope: SCOPE
    )
    authorizer.fetch_access_token!
  end

  def refresh_access_token!
    info = fetch_access_token
    update_attributes(
      access_token: info['access_token'],
      expires_in: Time.zone.now + info['expires_in'].seconds
    )
  end

  def expired?
    Time.zone.now > expires_in
  end
end
