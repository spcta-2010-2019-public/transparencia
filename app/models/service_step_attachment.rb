# frozen_string_literal: true
#
class ServiceStepAttachment < ApplicationRecord
  include CsvExportable
  include Versionable

  belongs_to :service_step

  # Validations
  # validates :service_step_id, presence: true
  validates :name,            presence: true
  validates :attachment,      presence: true

  has_attached_file :attachment
  do_not_validate_attachment_file_type :attachment

  def self.acts_as_label
    :name
  end
end
