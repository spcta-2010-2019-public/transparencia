# frozen_string_literal: true
#
class Menu < ApplicationRecord
  serialize :roles, Array
end
