# frozen_string_literal: true
#
class Service < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable
  include PgSearch
  multisearchable against: [:name, :description, :institution_data],
                  if: :enabled?

  belongs_to :institution
  belongs_to :institution_dependency
  belongs_to :services_category

  has_many :service_steps, dependent: :destroy

  has_many :service_attachments, dependent: :destroy
  accepts_nested_attributes_for :service_attachments, allow_destroy: true

  # Validations
  validates :institution_id, presence: true
  validates :description,    presence: true

  validates :name, presence: true
  validates :name,
            uniqueness: {
              case_sensitive: false,
              scope: [:institution_id, :institution_dependency_id]
            }

  # Scopes
  default_scope { order(acts_as_label) }
  scope :enabled, -> { where(enabled: true) }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def institution_data
    [institution.acronym, institution.name].join(' ')
  rescue
    ''
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def institution_dependency_id=(string)
    return super if number?(string)
    id_from_string(InstitutionDependency, string)
  end

  def services_category_id=(string)
    return super if number?(string)
    id_from_string(ServicesCategory, string)
  end

  def self.grid?
    true
  end
end
