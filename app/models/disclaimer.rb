class Disclaimer < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  belongs_to :institution_dependency
  belongs_to :information_standard

  # Validations
  validates :institution_id,          presence: true
  validates :information_standard_id, presence: true
  validates :name,                    presence: true
  validates :file,                    presence: true

  scope :enabled, -> { where(enabled: true) }

  has_attached_file :file
  do_not_validate_attachment_file_type :file

  default_scope { order(acts_as_label) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end


end
