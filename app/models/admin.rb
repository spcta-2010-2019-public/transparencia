# frozen_string_literal: true
#
class Admin < ApplicationRecord
  include CsvExportable
  include Versionable

  has_and_belongs_to_many :institutions

  has_many :partners,
           -> { where.not(role: Admin.roles[:admin]) },
           through: :institutions,
           source: :admins

  has_and_belongs_to_many :restricted_standards,
                          join_table: :admins_information_standars,
                          class_name: 'InformationStandard'

  has_many :institution_types,                   through: :institutions
  has_many :standard_categories,                 through: :institutions
  has_many :oir_addresses,                       through: :institutions
  has_many :institution_dependencies,            through: :institutions
  has_many :document_categories,                 through: :institutions
  has_many :services_categories,                 through: :institutions
  has_many :documents,                           through: :institutions
  has_many :services,                            through: :institutions
  has_many :service_steps,                       through: :services
  has_many :committees,                          through: :institutions
  has_many :officials,                           through: :institutions
  has_many :consultants,                         through: :institutions
  has_many :budgetary_units,                     through: :institutions
  has_many :work_lines,                          through: :budgetary_units
  has_many :remunerations,                       through: :institutions
  has_many :travels,                             through: :institutions
  has_many :executing_works,                     through: :institutions
  has_many :institution_inventory_categories,    through: :institutions
  has_many :institution_items,                   through: :institutions
  has_many :procurements,                        through: :institutions
  has_many :institution_recruitment_results,     through: :institutions
  has_many :institution_confidential_infos,      through: :institutions
  has_many :resources_to_private_recipients,     through: :institutions
  has_many :participation_mechanisms,            through: :institutions
  has_many :disclaimers,                         through: :institutions

  has_many :information_standards,
           lambda {
             where(
               information_standards_standard_categories: {
                 linked_institution_id: nil
               }
             )
           },
           through: :standard_categories

  has_one :admin_setting

  devise :database_authenticatable,
         :recoverable,
         :rememberable,
         :trackable,
         # :validatable,
         # :confirmable,
         :lockable

  enum role: [:admin, :oir, :hospital, :region]

  has_attached_file :avatar,
                    styles: {
                      small:    '40x40#',
                      medium:   '80x80#',
                      original: '120x120#'
                    }
  validates_attachment_content_type :avatar, content_type: /\Aimage/

  validates :name, presence: true

  validates :email, presence: true
  validates :email,
            uniqueness: { case_sensitive: false },
            if: proc { |o| o.email.present? }

  validates :email,
            format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i },
            if: proc { |o| o.email.present? }

  validates :password,
            presence: true,
            confirmation: true,
            on: :create

  validates :password,
            presence: true,
            confirmation: true,
            if: proc { |o| o.password.present? },
            on: :update

  validates :role, presence: true

  def self.acts_as_label
    :name
  end

  def one_institution?
    institutions.count == 1
  end

  def uniq_institution
    return nil unless one_institution?
    institutions.try(:first)
  end

  def restricted_standards?
    restricted_standards.count.positive?
  end
end
