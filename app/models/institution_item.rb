# frozen_string_literal: true
#
class InstitutionItem < ApplicationRecord
  include CsvExportable
  include Versionable

  belongs_to :institution

  validates :name, :description, :brand, :market_value, :acquisition_date, presence: true
  validates :year, :institution_id,
            presence: true,
            numericality: { only_integer: true }

  validates :market_value, presence: true
  validates :market_value,
            numericality: { greater_than: 0.0 },
            unless: proc { |o| o.market_value.blank? }

  has_attached_file :file
  do_not_validate_attachment_file_type :file

  def file_url
    return '' unless file.present?
    URI.join(ActionController::Base.asset_host, file.url)
  end

  scope :enabled, -> { where(enabled: true) }

  # ransacker :year, type: :string do
  #   Arel.sql('TO_CHAR("year", \'9999\')')
  # end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def self.grid?
    true
  end
end
