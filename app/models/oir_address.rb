# frozen_string_literal: true
#
class OirAddress < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  DEFAULT_COORDS = { lat: 13.7013328, lng: -89.2244275 }.freeze

  serialize :schedule, Array

  belongs_to :institution

  validates :institution_id, presence: true
  validates :address,        presence: true
  validates :phone,          presence: true

  def self.acts_as_label
    :address
  end

  ransacker :address, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"address\")")
  end

  def schedule_to_human(value)
    value.map do |v|
      next if v[:from].blank? || v[:to].blank?

      "#{I18n.t(v[:wday], scope: 'date.cday_names')} " \
      "#{I18n.t('from')} #{v[:from]} " \
      "#{I18n.t('to')} #{v[:to]}"
    end.compact.to_sentence
  end

  def schedule_to_human2(value)
    r = ""
    today = Time.now.strftime("%A").downcase
    value.map do |v|
      next if v[:from].blank? || v[:to].blank?
      r = r + (today == v[:wday] ? "<strong>" : "")
      r = r + "<div class='row'>#{I18n.t(v[:wday], scope: 'date.cday_names')} #{I18n.t('from')} #{v[:from]} #{I18n.t('to')} #{v[:to]}</div>"
      r = r + (today == v[:wday] ? "</strong>" : "")
    end
    r
  end

  def self.diff_methods
    { schedule: :schedule_to_human }
  end

  before_save :clear_imcomplete_schedule
  #
  def clear_imcomplete_schedule
    self.schedule = schedule.select { |s| s[:from].present? && s[:to].present? }
  end
end
