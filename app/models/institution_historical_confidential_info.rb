class InstitutionHistoricalConfidentialInfo < ApplicationRecord
  belongs_to :institution_confidential_info

  validates :comment, presence: true
end
