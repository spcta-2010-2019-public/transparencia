# frozen_string_literal: true
#
class Document < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable
  include PgSearch

  multisearchable against: [:name, :description, :institution_data],
                  if: :enabled?

  belongs_to :institution
  belongs_to :institution_dependency
  belongs_to :information_standard
  belongs_to :document_category

  # Validations
  validates :institution_id,          presence: true
  validates :information_standard_id, presence: true
  validates :name,                    presence: true
  validates :description,             presence: true
  validates :document,                presence: true

  # validates :priority, presence: true
  validates :priority,
            numericality: { only_integer: true },
            unless: proc { |o| o.priority.blank? }

  validates :year, presence: true
  validates :year,
            numericality: { only_integer: true, greater_than: 0 },
            length: { is: 4 },
            unless: proc { |o| o.year.blank? }

  scope :enabled, -> { where(enabled: true) }

  scope :marked, -> { where(marked: true) }

  default_scope { order('marked DESC, documents.name ASC') }

  scope :featured_resolutions, lambda {
    includes(:institution)
      .enabled
      .where(information_standard_id: 39)
      .where('documents.downloads >= 50')
      .reorder('RANDOM()')
      .limit(5)
  }
  # TODO, burn information_standard_id

  scope :top_downloaded, lambda {
    where('downloads > 0')
      .reorder('downloads DESC')
  }

  scope :top_institution_downloaded, lambda { |institution_ids|
    top_downloaded.where(institution_id: institution_ids)
  }

  ransacker :year, type: :string do
    Arel.sql('TO_CHAR("year", \'9999\')')
  end

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  ransacker :description, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"description\")")
  end

  default_scope { order(acts_as_label) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def file_extension
    n = document_file_name
    r = n.index('.') ? n.split('.') : ['', '???']
    r[-1].upcase
  rescue
    ''
  end

  has_attached_file :document
  do_not_validate_attachment_file_type :document

  def with_document
    document_file_name.present?
  end

  def information_standard_id=(string)
    return super if number?(string)
    id_from_string(InformationStandard, string)
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def institution_dependency_id=(string)
    return super if number?(string)
    id_from_string(InstitutionDependency, string)
  end

  def document_category_id=(string)
    return super if number?(string) || string.blank?

    is_name, dc_name = string.split('/')
    is_id = InformationStandard.where(name: is_name.strip).first.id
    dc_id = DocumentCategory
            .where(institution_id: institution_id,
                   name: dc_name.strip,
                   information_standard_id: is_id).first.id

    write_attribute :document_category_id, dc_id
  rescue
    nil
  end

  def self.grid?
    true
  end

  def title
    description.blank? ? name : description
  end

  def institution_data
    [institution.acronym, institution.name].join(' ')
  rescue
    ''
  end

  def download_url
    return '' unless document.present?
    Rails
      .application
      .routes
      .url_helpers
      .download_institution_document_url(institution, self)
  end

  def document_url
    ActionController::Base.helpers.asset_url(document.url(:original, timestamp: false))
  end

  def self.external_json_methods
    %w(document_url)
  end

end
