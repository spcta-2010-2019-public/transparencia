# frozen_string_literal: true
#
class Committee < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution

  validates :institution_id, presence: true

  validates :name, presence: true
  # validates :name, uniqueness: { case_sensitive: false } TODO

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  # Scopes
  default_scope { order(acts_as_label) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end
end
