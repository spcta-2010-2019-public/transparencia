# frozen_string_literal: true
#
module Institutionable
  extend ActiveSupport::Concern

  included do
    after_save :update_institution_model
  end

  def update_institution_model
    Institution.find(institution_id).touch
  rescue
    nil
  end
end
