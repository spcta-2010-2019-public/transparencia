# frozen_string_literal: true
#
class Remuneration < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  belongs_to :work_line
  has_one :budgetary_unit, through: :work_line

  ##
  # Validations
  validates :institution_id,  presence: true
  # validates :work_line_id, presence: true
  validates :salary_category, presence: true

  validates :name, presence: true
  validates :name,
            uniqueness: {
              case_sensitive: false,
              scope: [
                :institution_id,
                :work_line_id,
                :monthly_remuneration,
                :salary_category,
                :year
              ]
            },
            unless: proc { |o| o.name.blank? }

  validates :year, presence: true
  validates :year,
            numericality: { only_integer: true, greater_than: 0 },
            length: { is: 4 },
            unless: proc { |o| o.year.blank? }

  validates :employees_number, presence: true
  validates :employees_number,
            numericality: { only_integer: true, greater_than: 0 },
            unless: proc { |o| o.employees_number.blank? }

  validates :monthly_remuneration, presence: true
  validates :monthly_remuneration,
            numericality: { greater_than_or_equal_to: 0.0 },
            unless: proc { |o| o.monthly_remuneration.blank? }

  validates :expenses_remuneration, presence: true
  validates :expenses_remuneration,
            numericality: { greater_than_or_equal_to: 0.0 },
            unless: proc { |o| o.expenses_remuneration.blank? }

  ransacker :year, type: :string do
    Arel.sql('TO_CHAR("year", \'9999\')')
  end

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  ransacker :salary_category, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"salary_category\")")
  end

  default_scope { order(acts_as_label) }

  scope :enabled, -> { where(enabled: true) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def work_line_id=(string)
    return super if number?(string) || string.blank?

    bu_name, wl_name = string.split('/')
    bu_id = BudgetaryUnit
            .where(institution_id: institution_id, name: bu_name.try(:strip))
            .first.id

    wl_id = WorkLine
            .where(name: wl_name.try(:strip), budgetary_unit_id: bu_id).first.id

    write_attribute :work_line_id, wl_id
  rescue
    nil
  end

  def self.grid?
    true
  end
end
