# frozen_string_literal: true
# rubocop:disable ClassLength
class Travel < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  enum passage_payer: [
    :passage_institutional,
    :passage_cooperation,
    :passage_employee
  ]

  enum viatical_payer: [
    :viatical_institutional,
    :viatical_cooperation,
    :viatical_employee
  ]

  enum expenses_payer: [
    :expenses_institutional,
    :expenses_cooperation,
    :expenses_employee
  ]

  belongs_to :institution
  belongs_to :institution_dependency

  # Validations
  validates :name,             presence: true
  validates :institution_id,   presence: true
  validates :officer_name,     presence: true
  validates :officer_position, presence: true
  validates :destination,      presence: true
  validates :departure_date,   presence: true
  validates :return_date,      presence: true
  validates :objective,        presence: true

  validates :passage_payer,   presence:  true
  validates :viatical_payer,  presence:  true
  validates :expenses_payer,  presence:  true

  # passage validations
  validates :passage_amount, presence: true
  validates :passage_amount,
            numericality: { greater_than_or_equal_to: 0.0 },
            unless: proc { |o|
              o.passage_institutional? &&
                o.viatical_amount.to_f.zero? &&
                o.expenses_amount.to_f.zero?
            }
  validates :passage_amount,
            numericality: { greater_than: 0.0 },
            if: proc { |o|
              o.passage_institutional?
            }

  validates :passage_payer_comments,
            presence: true,
            if: proc { |o| o.passage_cooperation? || o.passage_employee? }

  # viatical validations
  validates :viatical_amount, presence: true
  validates :viatical_amount,
            numericality: { greater_than_or_equal_to: 0.0 },
            unless: proc { |o|
              o.passage_institutional? &&
                o.passage_amount.to_f.zero? &&
                o.expenses_amount.to_f.zero?
            }
  validates :viatical_amount,
            numericality: { greater_than: 0.0 },
            if: proc { |o|
              o.viatical_institutional?
            }

  validates :viatical_payer_comments,
            presence: true,
            if: proc { |o| o.viatical_cooperation? || o.viatical_employee? }

  # expenses validations
  validates :expenses_amount, presence: true
  validates :expenses_amount,
            numericality: { greater_than_or_equal_to: 0.0 },
            unless: proc { |o|
              o.passage_institutional? &&
                o.viatical_amount.to_f.zero? &&
                o.passage_amount.to_f.zero?
            }
  #validates :expenses_amount,
  #          numericality: { greater_than: 0.0 },
  #          if: proc { |o|
  #            o.expenses_institutional?
  #          }

  validates :expenses_payer_comments,
            presence: true,
            if: proc { |o| o.expenses_cooperation? || o.expenses_employee? }

  # Scopes
  default_scope { order(acts_as_label) }

  scope :enabled, -> { where(enabled: true) }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  ransacker :officer_name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"officer_name\")")
  end

  ransacker :destination, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"destination\")")
  end

  ransacker :departure_date, type: :date do
    Arel.sql('date(departure_date)')
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def institution_dependency_id=(string)
    return super if number?(string)
    id_from_string(InstitutionDependency, string)
  end

  def self.grid?
    true
  end

  def passage_payer=(param)
    val = self.class.passage_payers.keys.find do |i|
      param == I18n.t(i, scope: 'activerecord.enum.travel.passage_payer')
    end
    return write_attribute(:passage_payer, val) unless val.nil?
    super
  rescue
    nil
  end

  def viatical_payer=(param)
    val = self.class.viatical_payers.keys.find do |i|
      param == I18n.t(i, scope: 'activerecord.enum.travel.viatical_payer')
    end
    return write_attribute(:viatical_payer, val) unless val.nil?
    super
  rescue
    nil
  end

  def expenses_payer=(param)
    val = self.class.expenses_payers.keys.find do |i|
      param == I18n.t(i, scope: 'activerecord.enum.travel.expenses_payer')
    end
    return write_attribute(:expenses_payer, val) unless val.nil?
    super
  rescue
    nil
  end
end
