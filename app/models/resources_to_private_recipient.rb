# frozen_string_literal: true
#
class ResourcesToPrivateRecipient < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  belongs_to :institution_dependency

  # Validations
  validates :institution_id, presence: true
  validates :name,           presence: true

  validates :priority, presence: true
  validates :priority,
            numericality: { only_integer: true },
            unless: proc { |o| o.priority.blank? }

  enum execution_phase: [
    :in_action,
    :liquidation_process,
    :ended
  ]

  serialize :funding_source

  def self.funding_source_translation(funding_source_name)
    I18n.t(
      funding_source_name,
      scope: 'activerecord.enum.resources_to_private_recipient.funding_source'
    )
  end

  FUNDING_SOURCE = {
    'goverment'   => funding_source_translation('goverment'),
    'own_funds'   => funding_source_translation('own_funds'),
    'cooperation' => funding_source_translation('cooperation'),
    'other'       => funding_source_translation('other')
  }.freeze

  has_attached_file :attachment
  do_not_validate_attachment_file_type :attachment

  def attachment_url
    return '' unless attachment.present?
    URI.join(ActionController::Base.asset_host, attachment.url)
  end

  has_attached_file :funds_attachment
  do_not_validate_attachment_file_type :funds_attachment

  def funds_attachment_url
    return '' unless funds_attachment.present?
    URI.join(ActionController::Base.asset_host, funds_attachment.url)
  end

  scope :enabled, -> { where(enabled: true) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def execution_phase_string
    return '' unless execution_phase.present?

    I18n.t(
      execution_phase,
      scope: 'activerecord.enum.resources_to_private_recipient.execution_phase'
    )
  end

  def funding_source_string
    return '' unless funding_source.present?
    Array.wrap(funding_source).map { |n| FUNDING_SOURCE[n] }.join(', ')
  end

  def execution_phase=(param)
    scp = 'activerecord.enum.resources_to_private_recipient.execution_phase'
    val = self.class.execution_phases.keys.find do |i|
      param == I18n.t(i, scope: scp)
    end
    return write_attribute(:execution_phase, val) unless val.nil?
    super
  rescue
    nil
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def institution_dependency_id=(string)
    return super if number?(string)
    id_from_string(InstitutionDependency, string)
  end

  def self.grid?
    true
  end
end
