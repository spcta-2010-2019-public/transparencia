# frozen_string_literal: true
#
class InstitutionType < ApplicationRecord
  include CsvExportable
  include Versionable

  has_many :institutions

  validates :priority, presence: true
  validates :priority, numericality: { only_integer: true }

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  default_scope { order(:priority) }
  scope :enabled, -> { where(enabled: true) }
  scope :disabled, -> { where(enabled: true) }

  def self.acts_as_label
    :name
  end

  def how_many?
    Institution.enabled.where(:institution_type => id).count
  end
end
