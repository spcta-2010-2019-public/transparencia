# frozen_string_literal: true
#
class InformationStandardFrame < ApplicationRecord
  include CsvExportable
  include Versionable

  has_many :information_standards

  validates :priority, presence: true
  validates :priority, numericality: { only_integer: true }

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  default_scope { order(:priority) }

  def self.acts_as_label
    :name
  end
end
