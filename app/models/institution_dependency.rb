# frozen_string_literal: true
#
class InstitutionDependency < ApplicationRecord
  include CsvExportable
  include Versionable

  belongs_to :institution
  has_many   :consultants
  has_many   :documents
  has_many   :executing_works
  has_many   :services
  has_many   :travels
  has_many   :disclaimers

  validates :institution_id, presence: true

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  # Scopes
  default_scope { order(acts_as_label) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end
end
