# frozen_string_literal: true
#
class BudgetaryUnit < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  has_many   :work_lines

  # Validations
  validates :institution_id, presence: true

  validates :digit, presence: true
  validates :digit,
            numericality: { only_integer: true, greater_than: 0 }
  # uniqueness: { scope: :institution_id } TODO

  validates :name, presence: true
  # validates :name,
  # uniqueness: { case_sensitive: false, scope: :institution_id } TODO

  # Scopes
  default_scope { order(:digit) }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end
end
