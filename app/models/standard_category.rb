# frozen_string_literal: true
#
class StandardCategory < ApplicationRecord
  include CsvExportable
  include Versionable

  has_many :institutions
  has_many :information_standards, through: :standard_category_infos
  has_many :linked_institutions,
           through: :standard_category_infos,
           class_name: 'Institution'

  has_many :standard_category_infos, autosave: true
  accepts_nested_attributes_for :standard_category_infos,
                                allow_destroy: true,
                                reject_if: proc { |o|
                                  o[:id].blank? &&
                                    o[:information_standard_id].to_i.zero?
                                }

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  default_scope { order(acts_as_label) }

  def self.acts_as_label
    :name
  end

  before_save :mark_infos_for_removal
  #
  def mark_infos_for_removal
    standard_category_infos.each do |i|
      i.mark_for_destruction if i.information_standard_id.to_i.zero?
    end
  end
end
