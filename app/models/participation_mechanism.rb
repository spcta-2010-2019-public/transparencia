class ParticipationMechanism < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  belongs_to :institution_dependency

  # Validations
  validates :institution_id, presence: true
  validates :name,           presence: true

  scope :enabled, -> { where(enabled: true) }

  has_attached_file :report
  do_not_validate_attachment_file_type :report

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end
end
