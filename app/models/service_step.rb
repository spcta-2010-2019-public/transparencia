# frozen_string_literal: true
#
class ServiceStep < ApplicationRecord
  include CsvExportable
  include Versionable

  belongs_to :service

  has_many :service_step_attachments
  accepts_nested_attributes_for :service_step_attachments, allow_destroy: true

  # Validations
  validates :service_id,  presence: true
  validates :description, presence: true

  validates :name, presence: true
  validates :name,
            uniqueness: {
              case_sensitive: false,
              scope: :service_id
            }

  validates :correlative, presence: true
  validates :correlative,
            numericality: { only_integer: true, greater_than: 0 },
            uniqueness: { scope: :service_id }

  # Scopes
  default_scope { order(:service_id, :correlative) }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  def self.acts_as_label
    :name
  end
end
