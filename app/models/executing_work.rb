# frozen_string_literal: true
#
class ExecutingWork < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  belongs_to :institution_dependency

  validates :contract_code, presence: true
  validates :executing_company, presence: true
  validates :execution_time, presence: true
  validates :financing_source, presence: true
  validates :guarantee, presence: true
  validates :institution_id, presence: true
  validates :location, presence: true
  validates :name, presence: true
  validates :payment_method, presence: true
  validates :responsible, presence: true
  validates :starting_date, presence: true
  validates :supervising_company, presence: true

  validates :year, presence: true
  validates :year,
            numericality: { only_integer: true, greater_than: 0 },
            length: { is: 4 }

  validates :beneficiaries_number, presence: true
  validates :beneficiaries_number,
            numericality: { only_integer: true, greater_than: 0 }

  validates :amount, presence: true
  validates :amount, numericality: { greater_than: 0.0 }

  # ransacker :year, type: :string do
  #   Arel.sql('TO_CHAR("year", \'9999\')')
  # end

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  scope :enabled, -> { where(enabled: true) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end

  def institution_id=(string)
    return super if number?(string)
    id_from_string(Institution, string)
  end

  def institution_dependency_id=(string)
    return super if number?(string)
    id_from_string(InstitutionDependency, string)
  end

  def self.grid?
    true
  end
end
