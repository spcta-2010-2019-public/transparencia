class InstitutionInventoryCategory < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  has_many :institution_items, dependent: :destroy

  # Validations
  validates :institution_id,          presence: true
  validates :year, presence: true
  validates :year,
            numericality: { only_integer: true, greater_than: 0 },
            length: { is: 4 }

  default_scope { order(acts_as_label) }

  ransacker :year, type: :string do
    Arel.sql('TO_CHAR("year", \'9999\')')
  end

  def self.acts_as_label
    :year
  end

  def acts_as_label
    send(self.class.acts_as_label)
  end
end
