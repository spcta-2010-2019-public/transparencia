# frozen_string_literal: true
#
class WorkLine < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :budgetary_unit
  has_many   :remunerations

  # Validations
  validates :budgetary_unit_id, presence: true

  validates :digit, presence: true
  validates :digit,
            numericality: { only_integer: true }
  # , greater_than: 0 } TODO
  # uniqueness: { scope: :budgetary_unit_id } TODO

  validates :code, presence: true
  # validates :code,
  # uniqueness: { case_sensitive: false, scope: :budgetary_unit_id } TODO

  validates :name, presence: true
  # validates :name,
  # uniqueness: { case_sensitive: false, scope: :budgetary_unit_id } TODO

  # Scopes
  default_scope { order(acts_as_label) }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  def self.acts_as_label
    :name
  end

  def acts_as_label
    [budgetary_unit.acts_as_label, send(self.class.acts_as_label)].join(' / ')
  end
end
