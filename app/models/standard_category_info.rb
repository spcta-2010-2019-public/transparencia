# frozen_string_literal: true
#
class StandardCategoryInfo < ApplicationRecord
  #
  self.table_name = 'information_standards_standard_categories'
  #
  belongs_to :standard_category
  belongs_to :information_standard
  belongs_to :linked_institution, class_name: 'Institution'
end
