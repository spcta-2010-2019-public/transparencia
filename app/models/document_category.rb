# frozen_string_literal: true
#
class DocumentCategory < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution
  belongs_to :information_standard
  has_many   :documents, dependent: :nullify

  validates :institution_id, presence: true
  validates :information_standard_id, presence: true

  validates :priority, presence: true
  validates :priority, numericality: { only_integer: true }

  validates :name, presence: true
  # validates :name, uniqueness: { case_sensitive: false }

  ransacker :name, type: :string do
    Arel.sql("UNACCENT(\"#{table_name}\".\"name\")")
  end

  default_scope { order(acts_as_label) }

  def self.acts_as_label
    :name
  end

  def acts_as_label
    [
      information_standard.try(:acts_as_label),
      send(self.class.acts_as_label)
    ].compact.join(' / ')
  end
end
