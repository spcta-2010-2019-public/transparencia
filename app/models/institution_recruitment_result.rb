class InstitutionRecruitmentResult < ApplicationRecord
  include CsvExportable
  include Versionable
  include Institutionable

  belongs_to :institution

  validates :name, :job_url, :institution_id, presence: true


  default_scope { order(acts_as_label) }

  scope :enabled, -> { where(enabled: true) }

  def self.acts_as_label
    :name
  end
end
