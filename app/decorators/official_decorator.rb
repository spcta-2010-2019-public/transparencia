# frozen_string_literal: true
#
class OfficialDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def institution_dependency_id
    return '' if object.institution_dependency.nil?
    object.institution_dependency.name
  end

  def committee_id
    return '' if object.committee.nil?
    object.committee.name
  end

  def created_at
    h.l object.created_at, format: :localized
  end

  def updated_at
    h.l object.updated_at, format: :localized
  end

  def address
    h.strip_tags object.address
  end

  def functions
    h.strip_tags object.functions
  end

  def curriculum
    #ActionView::Base.full_sanitizer.sanitize(object.curriculum, :tags => %w(img br a), :attributes => %w(src style))
    h.strip_tags object.curriculum
  end


end
