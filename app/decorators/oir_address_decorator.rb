# frozen_string_literal: true
#
class OirAddressDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def schedule
    object.schedule_to_human(object.schedule)
  end
end
