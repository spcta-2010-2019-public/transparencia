# frozen_string_literal: true
#
class BudgetaryUnitDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end
end
