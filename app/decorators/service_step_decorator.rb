# frozen_string_literal: true
#
class ServiceStepDecorator < Draper::Decorator
  delegate_all

  def service_id
    return '' if object.service.nil?
    object.service.name
  end

  def created_at
    h.l object.created_at, format: :localized
  end

  def updated_at
    h.l object.updated_at, format: :localized
  end
end
