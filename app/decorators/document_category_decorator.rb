# frozen_string_literal: true
#
class DocumentCategoryDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def information_standard_id
    return '' if object.information_standard.nil?
    object.information_standard.name
  end

  def created_at
    h.l object.created_at, format: :localized
  end

  def updated_at
    h.l object.updated_at, format: :localized
  end
end
