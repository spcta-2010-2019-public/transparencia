# frozen_string_literal: true
#
class TravelDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def institution_dependency_id
    return '' if object.institution_dependency.nil?
    object.institution_dependency.name
  end

  def created_at
    h.l object.created_at, format: :localized
  end

  def updated_at
    h.l object.updated_at, format: :localized
  end

  def departure_date
    return object.departure_date unless context == :pdf
    h.l object.departure_date, format: :localized
  end

  def return_date
    return object.return_date unless context == :pdf
    h.l object.return_date, format: :localized
  end

  def passage_amount
    return object.passage_amount unless context == :pdf
    h.number_to_currency object.passage_amount
  end

  def viatical_amount
    return object.viatical_amount unless context == :pdf
    h.number_to_currency object.viatical_amount
  end

  def expenses_amount
    return object.expenses_amount unless context == :pdf
    h.number_to_currency object.expenses_amount
  end

  def passage_payer
    return '' if object.passage_payer.blank?
    h.t object.passage_payer, scope: 'activerecord.enum.travel.passage_payer'
  end

  def viatical_payer
    return '' if object.viatical_payer.blank?
    h.t object.viatical_payer, scope: 'activerecord.enum.travel.viatical_payer'
  end

  def expenses_payer
    return '' if object.expenses_payer.blank?
    h.t object.expenses_payer, scope: 'activerecord.enum.travel.expenses_payer'
  end
end
