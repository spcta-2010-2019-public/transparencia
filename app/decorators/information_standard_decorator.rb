# frozen_string_literal: true
#
class InformationStandardDecorator < Draper::Decorator
  delegate_all

  def information_standard_frame_id
    return '' if object.information_standard_frame.nil?
    object.information_standard_frame.name
  end
end
