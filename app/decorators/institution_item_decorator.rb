# frozen_string_literal: true
#
class InstitutionItemDecorator < Draper::Decorator
  delegate_all

  def institution_inventory_category_id
    "#{object.institution.name} - #{object.year}"
  end

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def acquisition_date
    return object.acquisition_date unless context == :pdf
    h.l object.acquisition_date, format: :localized
  end

  def market_value
    return object.market_value unless context == :pdf
    h.number_to_currency object.market_value
  end

  def current_value
    return object.current_value unless context == :pdf
    h.number_to_currency object.current_value
  end
end
