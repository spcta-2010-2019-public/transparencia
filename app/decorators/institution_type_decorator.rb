# frozen_string_literal: true
#
class InstitutionTypeDecorator < Draper::Decorator
  delegate_all

  def created_at
    h.l object.created_at, format: :localized
  end

  def updated_at
    h.l object.updated_at, format: :localized
  end
end
