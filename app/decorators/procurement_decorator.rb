# frozen_string_literal: true
#
class ProcurementDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def start_date
    return object.start_date unless context == :pdf
    h.l object.start_date, format: :localized
  end

  def amount
    return object.amount unless context == :pdf
    h.number_to_currency object.amount
  end

end
