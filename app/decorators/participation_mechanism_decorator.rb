# frozen_string_literal: true
#
class ParticipationMechanismDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def institution_dependency_id 
    return '' if object.institution_dependency.nil?
    object.institution_dependency.name
  end

  def execution_date_begin
    return object.execution_date_begin unless context == :pdf
    h.l object.execution_date_begin, format: :localized
  end

  def execution_date_end
    return object.execution_date_end unless context == :pdf
    h.l object.execution_date_end, format: :localized
  end


end
