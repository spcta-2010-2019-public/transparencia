# frozen_string_literal: true
#
class DocumentDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def institution_dependency_id
    return '' if object.institution_dependency.nil?
    object.institution_dependency.name
  end

  def information_standard_id
    return '' if object.information_standard.nil?
    object.information_standard.name
  end

  def document_category_id
    return '' if object.document_category.nil?
    object.document_category.acts_as_label
  end

  def created_at
    h.l object.created_at, format: :localized
  end

  def updated_at
    h.l object.updated_at, format: :localized
  end
end
