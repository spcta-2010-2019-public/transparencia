# frozen_string_literal: true
#
class ConsultantDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def institution_dependency_id
    return '' if object.institution_dependency.nil?
    object.institution_dependency.name
  end

  def applied_studies
    ActionView::Base.full_sanitizer.sanitize(object.applied_studies, :tags => %w(img a), :attributes => %w(src style))
  end

  def functions
    ActionView::Base.full_sanitizer.sanitize(object.functions, :tags => %w(img a), :attributes => %w(src style))
  end

  def work_experience
    ActionView::Base.full_sanitizer.sanitize(object.work_experience, :tags => %w(img a), :attributes => %w(src style))
  end



  def created_at
    h.l object.created_at, format: :localized
  end

  def updated_at
    h.l object.updated_at, format: :localized
  end
end
