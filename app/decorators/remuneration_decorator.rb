# frozen_string_literal: true
#
class RemunerationDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def work_line_id
    return '' if object.work_line.nil?
    object.work_line.acts_as_label
  end

  def budgetary_unit_id
    return '' if object.budgetary_unit.nil?
    object.budgetary_unit.name
  end

  def created_at
    h.l object.created_at, format: :localized
  end

  def updated_at
    h.l object.updated_at, format: :localized
  end
end
