# frozen_string_literal: true
#
class InstitutionDecorator < Draper::Decorator
  delegate_all

  def institution_type_id
    return '' if object.institution_type.nil?
    object.institution_type.name
  end

  def standard_category_id
    return '' if object.standard_category.nil?
    object.standard_category.name
  end

  def administrative_document_type
    return '' if object.administrative_document_type.blank?
    h.t object.administrative_document_type,
        scope: 'activerecord.enum.institution.administrative_document_type'
  end
end
