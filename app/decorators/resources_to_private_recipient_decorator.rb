# frozen_string_literal: true
#
class ResourcesToPrivateRecipientDecorator < Draper::Decorator
  delegate_all

  def institution_id
    return '' if object.institution.nil?
    object.institution.name
  end

  def institution_dependency_id
    return '' if object.institution_dependency.nil?
    object.institution_dependency.name
  end

  def resources_amount
    return object.resources_amount unless context == :pdf
    h.number_to_currency object.resources_amount
  end

  def period_begin
    return object.period_begin unless context == :pdf
    h.l object.period_begin, format: :localized
  end

  def period_end
    return object.period_end unless context == :pdf
    h.l object.period_end, format: :localized
  end

  def execution_phase
    return '' if object.execution_phase.blank?
    h.t(
      object.execution_phase,
      scope: 'activerecord.enum.resources_to_private_recipient.execution_phase'
    )
  end
end
