# frozen_string_literal: true
#
class WorkLineDecorator < Draper::Decorator
  delegate_all

  def budgetary_unit_id
    return '' if object.budgetary_unit.nil?
    object.budgetary_unit.name
  end
end
