#= require jquery
#= require jquery_ujs
#= require cocoon
#= require ckeditor/init
#= require angular
#= require angular-resource
#= require angular-animate
#= require ng-table
#= require checklist-model
#= require handsontable.full
#= require ngHandsontable
#= require moment-with-locales
#= require moment-range
#= require angular-mighty-datepicker
#= require panel/app
#= require panel/documents

@initMap = ->
  if document.getElementById('map')
    ele = document.getElementById('map')
    map = new google.maps.Map(ele, {
      center: {
        lat: parseFloat(ele.getAttribute('data-lat')),
        lng: parseFloat(ele.getAttribute('data-lng'))
      },
      zoom: 16,
      scrollwheel: false
    })

    marker = new google.maps.Marker({
      position: {
        lat: parseFloat(ele.getAttribute('data-lat')),
        lng: parseFloat(ele.getAttribute('data-lng'))
      },
      animation: google.maps.Animation.DROP,
      draggable: true,
      map: map
    })

    google.maps.event.addListener marker, 'dragend', ->
      document.getElementById('item_lat').value = marker.getPosition().lat()
      document.getElementById('item_lng').value = marker.getPosition().lng()

$ ->

  $('[data-toggable-from-select]').hide()
  $.each $('[data-toggable-from-select]'), (i, e) ->
    toggable = $(e)
    toggler  = $("##{toggable.data('toggler-id')}")
    hide_on  = $.map toggable.data('toggable-hide-on').split(','), (value) ->
      return value.replace(/ /g, '')

    toggler.on 'change', ->
      if $.inArray($(this).val(), hide_on) != -1
        toggable.hide()
      else
        toggable.show()
    toggler.trigger('change')


  $('[data-xhrpopup]').on 'ajax:complete', (event, xhr, settings) ->
    html = $(xhr.responseText).hide()
    $('body').append(html).addClass('overlayed')
    html.fadeIn 'fast'

    $('[data-close]').on 'click', (e) ->
      e.preventDefault()
      $(this).closest('.overlay').fadeOut 'fast', ->
        $(this).remove()
        $('.overlayed').removeClass('overlayed')
