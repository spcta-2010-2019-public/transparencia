update_document_categories_select = ->

  #url = "http://api.gobiernoabierto.gob.sv/dropdowns/#{$("#item_information_standard_id").val()}/information_standard_document_categories.json?callback=?"
  #url = "http://api.localhost.com:3000/dropdowns/#{$("#item_information_standard_id").val()}/information_standard_document_categories.json?callback=?"
  #url = "http://localhost:3000/api/dropdowns/#{$("#item_information_standard_id").val()}/information_standard_document_categories.json"

  $("#item_document_category_id")
    .find("option")
    .remove()

  ($("<option />").text("-- Seleccione una opcion --").val("")).appendTo($("#item_document_category_id"))

  if $("#item_information_standard_id").val() != "" || $("#item_institution_id").val() != ""
    $.getJSON "//www.transparencia.gob.sv/panel/documents/information_standard_document_categories.json?callback=?", { institution_id: $("#item_institution_id").val(), information_standard_id: $("#item_information_standard_id").val() }, (data) =>
    #$.getJSON "http://localhost:3000/panel/documents/information_standard_document_categories.json?callback=?", { institution_id: $("#item_institution_id").val(), information_standard_id: $("#item_information_standard_id").val() }, (data) =>
      if $.isEmptyObject(data)
        $("#item_document_category_id").fadeOut()
      else
        $("#item_document_category_id").fadeIn()
        $.each(data, (i, val) =>
          $("<option />")
            .val(i)
            .text(val)
            .appendTo($("#item_document_category_id"))
        )
$(document).ready ->

  $("#item_information_standard_id").on "change", ->
    update_document_categories_select()

  $("#item_institution_id").on "change", ->
    update_document_categories_select()
