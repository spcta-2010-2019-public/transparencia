#= require jquery
#= require jquery_ujs
#= require Chart.bundle
#= require chartkick
#= require sharer


$ ->
  $('[data-sharer]').Sharer()

  $('nav a').on 'click', (e) ->
    e.stopPropagation() unless $(this).hasClass('active')

  $('nav').on 'click', (e) ->
    if $(this).find('a:hidden').length > 0 || $(this).hasClass('active')
      e.preventDefault()
      $(this).toggleClass('active')
      $('body').toggleClass('overlayed')

  $('#jfilter').on 'keyup', ->
    r = new RegExp($(this).val(), 'i')
    $('.filtrable').hide()
    $('.filtrable').filter(->
      $(this)
        .text()
        .match(r)
    ).show()
