# frozen_string_literal: true
#
class InformationStandardPdf < InformationStandardTemplate
  def initialize(item, fields, view)
    super(item, view)

    @item   = item
    @view   = view
    @fields = fields

    draw_content
  end

  def draw_content
    font_size 9
    company_logo
    document_name @item.acts_as_label
    company_name
    division_name
    content
    creation_datetime
    numerate_pages
  end

  def table_info
    @fields.map do |f|
      next if @item.decorate.send(f).blank? or (@item.send(f).class == String && @item.send(f).size > 1000)
      [
        I18n.t("activerecord.attributes.#{@item.class.to_s.underscore}.#{f}") +
          ':',
        @item.decorate(context: :pdf).send(f)
      ]

    end.compact
  end

  def other_info
    @info = []
    @fields.each do |f|
      next if @item.decorate.send(f).blank? or @item.send(f).class == Date or @item.send(f).class == DateTime or @item.send(f).class == Float or @item.send(f).class == BigDecimal or @item.send(f).class == Fixnum or (@item.send(f).size < 3000)
      splitted = @item.decorate.send(f)
      word = splitted.scan(/.{2000}/m)
      word.each do |chunk|
        @info << [
          I18n.t("activerecord.attributes.#{@item.class.to_s.underscore}.#{f}") +
          ': ' + "\n\n#{chunk.size}" +
          chunk.to_s
        ]
      end
    end
    @info.compact
  end

  def content
    move_down 90

    table(
      table_info,
      width: bounds.width,
      cell_style: {
        size: 11,
        border_width: 0,
        font: "#{Rails.root}/app/assets/fonts/courier.ttf"
      },
    )
    unless other_info.blank?
      table(
        other_info,
        width: bounds.width,
        cell_style: {
          size: 10,
          border_width: 0,
          font: "#{Rails.root}/app/assets/fonts/courier.ttf"
        }
      )
    end
  end
end
