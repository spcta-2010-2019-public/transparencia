# frozen_string_literal: true
# Template for pdf information standards
class InformationStandardTemplate < Prawn::Document
  def initialize(item, view)
    super()

    @item = item
    @view = view
    font "#{Rails.root}/app/assets/fonts/roboto.ttf"
    font_size 8
  end

  def numerate_pages
    font_size 10
    pages_string = '<page>/<total>'

    number_pages pages_string,
                 at: [bounds.right - 150, 0],
                 align: :right,
                 color: '333333',
                 width: 150,
                 page_filter: :all
  end

  def creation_datetime
    font_size 10
    number_pages I18n.l(Time.zone.now, format: :localized),
                 at: [0, 0],
                 color: '333333',
                 width: 150,
                 page_filter: :all
  end

  def company_logo
    image "#{Rails.root}/app/assets/images/logo.jpg", height: 50
  end

  def document_name(name)
    font_size 14
    width = 420
    text_box name,
             at: [(bounds.right / 2) - (width / 2), bounds.top - 55],
             width: width,
             align: :center
  end

  def company_name
    font_size 17
    width = 320
    text_box 'transparencia.gob.sv',
             at: [(bounds.right / 2) - (width / 2), bounds.top - 10],
             width: width,
             align: :center
  end

  def division_name
    font_size 9
    width = 400
    text_box @item.institution.name,
             at: [(bounds.right / 2) - (width / 2), bounds.top - 30],
             width: width,
             align: :center,
             leading: 2
  end

  def company_info
    font_size 8
    info =  ''

    width = 320
    text_box info,
             at: [(bounds.right / 2) - (width / 2), bounds.top - 30],
             width: width,
             align: :center,
             leading: 2
  end
end
