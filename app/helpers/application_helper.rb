# frozen_string_literal: true
#
module ApplicationHelper
  #
  def summarized_text(text, length, more)
    splitted = auto_link(text).html_safe.split

    return auto_link(text) unless splitted.length > length
    return "#{splitted[0...length].join(' ')}..." unless more

    content_tag :details, class: (more ? 'more' : nil) do
      (content_tag(:summary, splitted[0...length].join(' ').html_safe) +
       splitted[length...splitted.length].join(' ').html_safe).html_safe
    end
  end

end
