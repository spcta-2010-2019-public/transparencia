# require 'paperclip'
#
# ActiveRecord::Base.configurations = Rails.application.config.database_configuration
#
# ###
# # CONSTANTS
# ###
# old_root = '/home/www/vhosts/gobiernoabierto/current'
#
# ###
# # RAKE PROCESS
# ###
# namespace :migration do
#   desc "Migrate data from Old Site"
#   task :run => :environment do
#
#     ###
#     # Migrate InstitutionTypes
#     ###
#     puts "========== INSTITUTION TYPE =========="
#     OldInstitutionType.all.each do |it|
#       begin
#         o = InstitutionType.where(id: it.id).first_or_initialize
#         # if o.new_record?
#           o.name  = it.name
#           o.enabled  = 1
#           o.priority  = 0
#           o.created_at = it.created_at || Time.current
#           o.updated_at = it.updated_at || Time.current
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate Standard Categories
#     ###
#     puts "========== STANDARD CATEGORY =========="
#     OldStandardCategory.all.each do |sc|
#       begin
#         o = StandardCategory.where(id: sc.id).first_or_initialize
#         # if o.new_record?
#           o.name  = sc.name
#           o.created_at = sc.created_at
#           o.updated_at = sc.updated_at
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate institutions
#     ###
#     puts "========== INSTITUTION =========="
#     OldInstitution.find_in_batches do |institutions|
#       institutions.each do |institution|
#         begin
#           i = Institution.where(id: institution.id).first_or_initialize
#           # if i.new_record?
#             # Institution is new, we create it
#             officer = OldInformationOfficer.where(institution_id: institution.id).first
#             i.name      = institution.name
#             i.acronym   = institution.acronym
#             i.avatar    = institution.logo_path_file(old_root) ? File.open(institution.logo_path_file(old_root)) : nil
#             i.institution_type_id   = institution.institution_type_id
#             i.standard_category_id  = institution.information_standard_category_id
#             i.accepts_online_requests = institution.accepts_online_information_requests
#             i.administrative_document_type = institution.administrative_document_type
#             i.certification_amount = institution.certification_amount || 0
#             i.reproduction_amount = institution.reproduction_amount || 0
#             i.external_transparency_site_url = institution.transparency_external_portal_url
#             if officer
#               i.officer_email = officer.email
#               i.officer_name  = officer.name
#               i.officer_designation = officer.appointment_path_file(old_root) ? File.open(officer.appointment_path_file(old_root)) : nil
#             end
#             i.administrative_document = institution.administrative_record_path_file(old_root) ? File.open(institution.administrative_record_path_file(old_root)) : nil
#             i.enabled = true
#             unless i.save(validate: false)
#               log_message i.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate oir addresses
#     ###
#     puts "========== INFORMATION OFFICER =========="
#     OldInformationOfficer.find_in_batches do |officers|
#       officers.each do |ofc|
#         begin
#           address = OirAddress.where(institution_id: ofc.institution_id).first_or_initialize
#           # if address.new_record?
#             address.enabled = true
#             address.lat = ofc.latitude
#             address.lng = ofc.longitude
#             address.phone = ofc.phone
#             address.address = ofc.address
#             unless address.save(validate: false)
#               log_message address.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Information Standard Frames
#     ###
#     puts "========== INFORMATION STANDARD FRAME =========="
#     OldInformationStandardFrame.all.each do |sf|
#       begin
#         o = InformationStandardFrame.where(id: sf.id).first_or_initialize
#         # if o.new_record?
#           o.priority  = 0
#           o.name  = sf.name
#           o.created_at = sf.created_at
#           o.updated_at = sf.updated_at
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate Information Standard
#     ###
#     puts "========== INFORMATION STANDARD =========="
#     OldInformationStandard.all.each do |sf|
#       begin
#         o = InformationStandard.where(id: sf.id).first_or_initialize
#         # if o.new_record?
#           o.information_standard_frame_id  = sf.information_standard_frame_id
#           o.priority  = sf.priority
#           o.name  = sf.name
#           o.short_name  = sf.acronym
#           o.created_at = sf.created_at
#           o.updated_at = sf.updated_at
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate Institution Dependencies
#     ###
#     puts "========== INSTITUTION DEPENDENCY =========="
#     OldInstitutionDependency.all.each do |idep|
#       begin
#         o = InstitutionDependency.where(id: idep.id).first_or_initialize
#         # if o.new_record?
#           o.name  = idep.name
#           o.institution_id  = idep.institution_id
#           o.created_at = idep.created_at
#           o.updated_at = idep.updated_at
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate Institution Consultants
#     ###
#     puts "========== CONSULTANT =========="
#     OldConsultant.find_in_batches do |consultants|
#       consultants.each do |consultant|
#         begin
#           c = Consultant.where(id: consultant.id).first_or_initialize
#           # if c.new_record?
#             c.institution_id  = consultant.institution_id
#             c.name  = consultant.name
#             c.position  = consultant.job || ''
#             c.remuneration  = consultant.net_salary || 0
#             c.phone  = consultant.phone || ''
#             c.email  = consultant.email || ''
#             c.description  = consultant.description
#             c.applied_studies  = consultant.technical_information
#             c.work_experience  = consultant.experience
#             c.functions  = consultant.functions
#             c.enabled = consultant.active
#             c.active  = consultant.active
#             unless c.save(validate: false)
#               log_message c.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Committees
#     ###
#     puts "========== COMMITTEE =========="
#     OldCommittee.all.each do |comm|
#       begin
#         o = Committee.where(id: comm.id).first_or_initialize
#         # if o.new_record?
#           o.name  = comm.name
#           o.institution_id  = comm.institution_id
#           o.created_at = comm.created_at
#           o.updated_at = comm.updated_at
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate Budgetary units
#     ###
#     puts "========== BUDGETARY UNITS =========="
#     OldBudgetaryUnit.all.each do |bu|
#       BudgetaryUnit.where(id: bu.id).first_or_create(institution_id: bu.institution_id, name: bu.name, digit: bu.digit)
#     end
#
#     ###
#     # Migrate Work Lines
#     ###
#     puts "========== WORK LINES =========="
#     OldWorkLine.all.each do |wl|
#       WorkLine.where(id: wl.id).first_or_create(budgetary_unit_id: wl.budget_unit_id, code: wl.code, name: wl.name, digit: wl.digit)
#     end
#
#     ###
#     # Migrate Executing Works
#     ###
#     puts "========== EXECUTING WORKS =========="
#     OldExecutingWork.find_in_batches do |works|
#       works.each do |work|
#         begin
#           w = ExecutingWork.where(id: work.id).first_or_initialize
#           # if w.new_record?
#             w.institution_id  = work.institution_id
#             w.starting_date  = work.start_date
#             w.amount  = work.total_cost
#             w.beneficiaries_number  = work.beneficiaries_number
#             w.executing_company  = work.implementing_company
#             w.execution_time  = work.execution_time
#             w.financing_source  = work.founding_source
#             w.location  = work.location
#             w.name  = work.name
#             w.responsible = work.work_responsible
#             w.supervising_company = work.supervising_company
#             w.contract_code = ''
#             w.guarantee = work.contract_content
#             w.payment_method = ''
#             w.enabled = true
#             w.active  = true
#             w.year  = w.starting_date.try(:year) || Date.current.year
#             w.created_at = work.created_at
#             w.updated_at = work.updated_at
#             unless w.save(validate: false)
#               log_message w.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Remunerations
#     ###
#     puts "========== REMUNERATIONS =========="
#     OldRemuneration.find_in_batches do |remunerations|
#       remunerations.each do |obj|
#         begin
#           o = Remuneration.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#             o.institution_id  = obj.institution_id
#             o.work_line_id  = obj.line_of_work_id
#             o.ad_honorem  = obj.honorary || false
#             o.name  = obj.name
#             o.employees_number = obj.total_employee
#             o.monthly_remuneration  = obj.individual_monthly_remuneration
#             o.expenses_remuneration = obj.other_remuneration
#             o.salary_category = obj.wage_category
#             o.enabled = true
#             o.active  = true
#             o.created_at = obj.created_at || Time.current
#             o.updated_at = obj.updated_at || Time.current
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Travels
#     ###
#     puts "========== TRAVELS =========="
#     OldTravel.find_in_batches do |travels|
#       travels.each do |obj|
#         begin
#           o = Travel.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#             o.institution_id  = obj.institution_id
#             o.name  = obj.name
#             o.officer_name  = obj.institution_official_name
#             o.officer_position = obj.institution_official_job || ''
#             o.destination  = obj.destination
#             o.passage_amount  = obj.travel_cost
#             o.viatical_amount = obj.viatical_cost
#             # o.expenses_amount = obj.others_cost
#             o.departure_date = obj.start_date
#             o.return_date = obj.end_date
#             o.objective = obj.objetive
#             o.enabled = true
#             o.observations = obj.sponsor_contribution || ''
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Officials
#     ###
#     puts "========== OFFICIALS =========="
#     OldOfficial.find_in_batches do |officials|
#       officials.each do |obj|
#         begin
#           o = Official.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#             o.institution_id  = obj.institution_id
#             o.committee_id  = obj.committee_id
#             o.name  = obj.name
#             o.position = obj.job || ''
#             o.phone  = obj.phone
#             o.email  = obj.email
#             o.address = obj.address
#             o.functions = obj.description
#             o.curriculum = obj.curriculum
#             o.enabled = obj.active
#             o.active = obj.active
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Document Categories
#     ###
#     puts "========== DOCUMENT CATEGORIES =========="
#     OldDocumentCategory.find_in_batches do |categories|
#       categories.each do |obj|
#         begin
#           o = DocumentCategory.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#             o.institution_id  = obj.institution_id
#             o.information_standard_id  = obj.information_standard_id
#             o.name  = obj.name
#             o.priority = obj.priority || 0
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Documents
#     ###
#     puts "========== DOCUMENTS =========="
#     OldDocument.find_in_batches do |documents|
#       documents.each do |obj|
#         begin
#           o = Document.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#             o.institution_id  = obj.institution_id
#             o.institution_dependency_id  = obj.institution_dependence_id
#             o.information_standard_id  = obj.information_standard_id
#             o.priority = obj.priority || 0
#             o.year = obj.year || obj.updated_at.year
#             o.name = obj.name
#             o.description = obj.description
#             o.document_category_id = obj.document_category_id
#             o.document = (obj.datafile_path_file(old_root) && File.exist?(obj.datafile_path_file(old_root))) ? File.open(obj.datafile_path_file (old_root)) : nil
#             o.downloads = obj.downloads
#             o.enabled = obj.active
#             o.active = obj.active
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Services Categories
#     ###
#     puts "========== SERVICE CATEGORIES =========="
#     OldServicesCategory.all.each do |sc|
#       begin
#         o = ServicesCategory.where(id: sc.id).first_or_initialize
#         # if o.new_record?
#           o.name = sc.name
#           o.institution_id = sc.institution_id
#           o.created_at = sc.created_at
#           o.updated_at = sc.updated_at
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate Services
#     ###
#     puts "========== SERVICES =========="
#     OldService.find_in_batches do |services|
#       services.each do |obj|
#         begin
#           o = Service.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#
#             o.institution_id  = obj.institution_id
#             o.name  = obj.name
#             o.response_time = obj.response_times
#             o.responsible_area = ''
#             o.responsible_name = obj.responsible
#             o.description = obj.description
#             o.requirements = obj.general_requirements
#             o.observations = obj.observations
#             o.responsible_area = obj.address
#             o.address = obj.request_address
#             o.schedule = obj.schedule
#             o.services_category_id = obj.institution_service_category_id
#             o.cost = OldServiceStep.where(institution_service_id: obj.id).pluck(:cost).sum
#
#             o.enabled = obj.active
#             o.active = obj.active
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Service Steps
#     ###
#     puts "========== SERVICE STEPS =========="
#     OldServiceStep.find_in_batches do |steps|
#       steps.each do |obj|
#         begin
#           o = ServiceStep.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#             o.service_id  = obj.institution_service_id
#             o.correlative  = obj.priority || 0
#             o.name = obj.name
#             o.responsible_name = obj.responsible
#             o.duration = obj.duration
#             o.response_time = obj.response_time
#             o.description = obj.description
#             o.schedule = obj.schedule
#             o.requirements = obj.requirements
#             o.documents_to_present = obj.documents
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Service Attachments
#     ###
#     puts "========== SERVICE ATTACHMENTS =========="
#     OldServiceAttachment.find_in_batches do |attachments|
#       attachments.each do |obj|
#         begin
#           if obj.attachable_type == 'InstitutionService'
#             o = ServiceAttachment.where(id: obj.id).first_or_initialize
#             o.service_id  = obj.attachable_id
#           else
#             o = ServiceStepAttachment.where(id: obj.id).first_or_initialize
#             o.service_step_id  = obj.attachable_id
#           end
#           # if o.new_record?
#             o.name  = obj.name
#             o.description  = obj.description
#             o.attachment = (obj.attachment_path_file(old_root) && File.exist?(obj.attachment_path_file(old_root))) ? File.open(obj.attachment_path_file(old_root)) : nil
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Institution Items
#     ###
#     puts "========== INSTITUTION ITEMS =========="
#     OldInstitutionItem.find_in_batches do |items|
#       items.each do |obj|
#         begin
#           o         = InstitutionItem.where(id: obj.id).first_or_initialize
#           category  = OldInstitutionInventoryCategory.where(id: obj.institution_inventory_category_id).first rescue nil
#           # if o.new_record?
#             o.year  = category.try(:year)
#             o.institution_id  = category.try(:institution_id)
#             o.name  = obj.name
#             o.description  = obj.description
#             o.brand  = obj.brand
#             o.market_value  = obj.market_value
#             o.current_value  = obj.current_value
#             o.acquisition_date  = obj.created_at.try(:to_date)
#             o.file  = (obj.file_path_file(old_root) && File.exist?(obj.file_path_file(old_root))) ? File.open(obj.file_path_file(old_root)) : nil
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             o.enabled = true
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Procurements
#     ###
#     puts "========== PROCUREMENTS =========="
#     OldProcurement.find_in_batches do |items|
#       items.each do |obj|
#         begin
#           o = Procurement.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#             o.institution_id  = obj.institution_id
#             o.name  = obj.name
#             o.amount  = obj.amount
#             o.form_of_contract  = obj.form_of_contract
#             o.counterpart_info  = obj.counterpart_info
#             o.compliance_deadlines  = obj.compliance_deadlines
#             o.attachment  = (obj.attachment_path_file(old_root) && File.exist?(obj.attachment_path_file(old_root))) ? File.open(obj.attachment_path_file(old_root)) : nil
#             o.start_date = obj.start_date
#             o.counterpart_name = obj.counterpart_name
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             o.enabled = true
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Resources to Private Recipients
#     ###
#     puts "========== RESOURCES TO PRIVATES =========="
#     OldResourcesToPrivateRecipient.find_in_batches do |items|
#       items.each do |obj|
#         begin
#           o = ResourcesToPrivateRecipient.where(id: obj.id).first_or_initialize
#           # if o.new_record?
#             o.institution_id  = obj.institution_id
#             o.name  = obj.name
#             o.allocation_of_resources  = obj.allocation_of_resources
#             o.resources_amount  = obj.resources_amount
#             o.period_begin  = obj.period_begin
#             o.period_end = obj.period_end
#             o.institution_dependency_id  = obj.institution_dependence_id
#
#             o.attachment  = (obj.attachment_path_file(old_root) && File.exist?(obj.attachment_path_file(old_root))) ? File.open(obj.attachment_path_file(old_root)) : nil
#             o.funds_attachment  = (obj.funds_attachment_path_file(old_root) && File.exist?(obj.funds_attachment_path_file(old_root))) ? File.open(obj.funds_attachment_path_file(old_root)) : nil
#
#             o.created_at = obj.created_at
#             o.updated_at = obj.updated_at
#             o.enabled = true
#             unless o.save(validate: false)
#               log_message o.errors.full_messages
#             end
#           # end
#         rescue Exception => e
#           log_message e
#         end
#       end
#     end
#
#     ###
#     # Migrate Institution Recruitment Results
#     ###
#     puts "========== INSTITUTIONS RECRUITMENTS =========="
#     OldInstitutionRecruitmentResult.all.each do |obj|
#       begin
#         o = InstitutionRecruitmentResult.where(id: obj.id).first_or_initialize
#         # if o.new_record?
#           o.institution_id = obj.institution_id
#           o.name = obj.name
#           o.job_url = obj.job_url
#           o.created_at = obj.created_at
#           o.updated_at = obj.updated_at
#           o.enabled = true
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate Information Standard Statistic
#     ###
#     puts "========== INFORMATION STANDARD STATISTICS =========="
#     OldInformationStandardStatistic.all.each do |obj|
#       begin
#         o = InformationStandardStatistic.where(id: obj.id).first_or_initialize
#         # if o.new_record?
#           o.institution_id = obj.institution_id
#           o.information_standard_id = obj.information_standard_id
#           o.sessions = obj.sessions
#           o.views = obj.views
#           o.tracked_date = obj.tracked_date
#           o.created_at = obj.created_at
#           o.updated_at = obj.updated_at
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate Information Standard Statistic Tracks
#     ###
#     puts "========== INFORMATION STANDARD STATISTICS TRACKS =========="
#     OldInformationStandardStatisticTrack.all.each do |obj|
#       begin
#         o = InformationStandardStatisticTrack.where(id: obj.id).first_or_initialize
#         # if o.new_record?
#           o.institution_id = obj.institution_id
#           o.information_standard_id = obj.information_standard_id
#           o.session_id = obj.session_id
#           o.tracked_date = obj.tracked_date
#           o.created_at = obj.created_at
#           o.updated_at = obj.updated_at
#           unless o.save(validate: false)
#             log_message o.errors.full_messages
#           end
#         # end
#       rescue Exception => e
#         log_message e
#       end
#     end
#
#     ###
#     # Migrate officers users
#     ###
#     puts "========== INFORMATION OFFICERS =========="
#     OldInstitution.order(:id).each do |ins|
#       officer = OldInformationOfficer.where(institution_id: ins.id).first
#       if officer
#         user    = OldUser.where(email: officer.email).first
#         ins_new = Institution.where(id: ins.id).first
#         if user and ins_new
#           encrypted_password  = user.encrypted_password
#           a       = Admin.where(email: user.email).first_or_initialize
#           a.name  = user.name
#           a.email = user.email
#           a.password  = user.email
#           a.role      = 'oir'
#           if a.save(validation: false)
#             a.update_attribute(:encrypted_password, encrypted_password)
#             if a.institution_ids.size == 0
#               ActiveRecord::Base.connection.execute("insert into admins_institutions values(#{a.id}, #{ins.id})")
#             end
#           else
#             log_message "Usuario no se guardo: #{user.id} - #{a.errors.full_messages}"
#           end
#         end
#       end
#     end
#     ###
#     # Correct id sequences
#     ###
#     ActiveRecord::Base.connection.tables.each do |table|
#       begin
#         ActiveRecord::Base.connection.execute("SELECT setval('#{table}_id_seq', COALESCE((SELECT MAX(id)+1 FROM #{table}), 1), false);")
#       rescue
#         # do nothing
#       end
#     end
#
#   end
# end
#
#
# ###
# # LOG METHOD
# ###
#
# def log_message message
#   open("#{Rails.root.to_s}/log/migration.log", 'a') do |f|
#     f.puts ""
#     f.puts "-----------------------------"
#     f.puts "#{message}"
#   end
# end
#
#
# ###
# # DEFINE OLD CLASS
# ###
#
# class OldUser < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'users'
# end
#
# class OldInformationStandardStatistic < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'information_standard_statistics'
# end
#
# class OldInformationStandardStatisticTrack < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'information_standard_statistic_tracks'
# end
#
# class OldInstitutionRecruitmentResult < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_recruitment_results'
# end
#
# class OldInstitutionInventoryCategory < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_inventory_categories'
# end
#
# class OldResourcesToPrivateRecipient < ActiveRecord::Base
#   include Paperclip::Glue
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'resources_to_private_recipients'
#
#   has_attached_file :attachment
#   def attachment_path_file old_root
#     @attachment_path_file ||= attachment.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_resources_to_private_recipients', 'resources_to_private_recipients')
#   end
#
#   has_attached_file :funds_attachment
#   def funds_attachment_path_file old_root
#     @funds_attachment_path_file ||= funds_attachment.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_resources_to_private_recipients', 'resources_to_private_recipients')
#   end
# end
#
# class OldInstitutionItem < ActiveRecord::Base
#   include Paperclip::Glue
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_items'
#
#   has_attached_file :file
#   def file_path_file old_root
#     @file_path_file ||= file.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_institution_items', 'institution_items')
#   end
# end
#
# class OldProcurement < ActiveRecord::Base
#   include Paperclip::Glue
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'procurements'
#
#   has_attached_file :attachment
#   def attachment_path_file old_root
#     @attachment_path_file ||= attachment.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_procurements', 'procurements')
#   end
# end
#
# class OldInstitutionType < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_types'
# end
#
# class OldInstitution < ActiveRecord::Base
#   include Paperclip::Glue
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institutions'
#
#   has_attached_file :logo
#   def logo_path_file old_root
#     @logo_path_file ||= logo.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_institutions', 'institutions')
#   end
#   has_attached_file :administrative_record
#   def administrative_record_path_file old_root
#     @administrative_record_path_file ||= administrative_record.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_institutions', 'institutions')
#   end
# end
#
# class OldInformationOfficer < ActiveRecord::Base
#   include Paperclip::Glue
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'information_officers'
#
#   has_attached_file :appointment_attachment
#   def appointment_path_file old_root
#     @appointment_path_file ||= appointment_attachment.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_information_officers', 'information_officers')
#   end
# end
#
# class OldStandardCategory < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'information_standard_categories'
# end
#
# class OldInformationStandardFrame < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'information_standard_frames'
# end
#
# class OldInformationStandard < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'information_standards'
# end
#
# class OldInstitutionDependency < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_dependences'
# end
#
# class OldConsultant < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_consultants'
# end
#
# class OldCommittee < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'committees'
# end
#
# class OldWorkLine < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'line_of_works'
# end
#
# class OldBudgetaryUnit < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'budget_units'
# end
#
# class OldExecutingWork < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'executing_works'
# end
#
# class OldRemuneration < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_remunerations'
# end
#
# class OldTravel < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_travels'
# end
#
# class OldOfficial < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_officials'
# end
#
# class OldDocumentCategory < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'document_categories'
# end
#
# class OldDocument < ActiveRecord::Base
#   include Paperclip::Glue
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'documents'
#
#   has_attached_file :datafile
#   def datafile_path_file old_root
#     @datafile_path_file ||= datafile.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_documents', 'documents')
#   end
# end
#
# class OldServicesCategory < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_service_categories'
# end
#
# class OldService < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_services'
# end
#
# class OldServiceStep < ActiveRecord::Base
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_service_steps'
# end
#
# class OldServiceAttachment < ActiveRecord::Base
#   include Paperclip::Glue
#   self.abstract_class = true
#   establish_connection 'old_version'.to_sym
#   self.table_name = 'institution_service_attachments'
#
#   has_attached_file :attachment
#   def attachment_path_file old_root
#     @attachment_path_file ||= attachment.path.try(:gsub, Rails.root.to_s, old_root).try(:gsub, 'old_service_attachments', 'institution_service_attachments')
#   end
# end
