# frozen_string_literal: true
#
namespace :menu do
  desc 'Loads menus table'
  task load: :environment do
    a = 'admin'
    o = 'oir'
    h = 'hospital'
    r = 'region'

    menus = [
      { controller: 'admins',                          action: 'index', roles: [a],          icon_name: 'people'            },
      { controller: 'information_standards',           action: 'index', roles: [a],          icon_name: 'info_outline'      },
      { controller: 'standard_categories',             action: 'index', roles: [a],          icon_name: 'info_outline'      },
      { controller: 'information_standard_frames',     action: 'index', roles: [a],          icon_name: 'info_outline'      },
      { controller: 'institutions',                    action: 'index', roles: [a, o, h, r], icon_name: 'business'          },
      { controller: 'institution_types',               action: 'index', roles: [a],          icon_name: 'business'          },
      { controller: 'consultants',                     action: 'index', roles: [o],          icon_name: 'person_outline',      information_standard_id: Config[:dynamical_information_standards][:institution_consultants] },
      { controller: 'procurements',                    action: 'index', roles: [o, h, r],    icon_name: 'attach_money',        information_standard_id: Config[:dynamical_information_standards][:institution_procurements] },
      { controller: 'documents',                       action: 'index', roles: [o, h, r],    icon_name: 'insert_drive_file' },
      { controller: 'document_categories',             action: 'index', roles: [o, h, r],    icon_name: 'insert_drive_file' },
      { controller: 'officials',                       action: 'index', roles: [o, h, r],    icon_name: 'people_outline',      information_standard_id: Config[:dynamical_information_standards][:institution_officials] },
      { controller: 'committees',                      action: 'index', roles: [o, h, r],    icon_name: 'people_outline',      information_standard_id: Config[:dynamical_information_standards][:institution_officials] },
      { controller: 'institution_dependencies',        action: 'index', roles: [o],          icon_name: 'group_work'        },
      { controller: 'oir_addresses',                   action: 'index', roles: [o, h, r],    icon_name: 'group_work'        },
      { controller: 'institution_items',               action: 'index', roles: [o, h, r],    icon_name: 'account_balance',     information_standard_id: Config[:dynamical_information_standards][:institution_inventory_categories] },
      { controller: 'participation_mechanisms',        action: 'index', roles: [o, h, r],    icon_name: 'people_outline',      information_standard_id: Config[:dynamical_information_standards][:participation_mechanisms] },
      { controller: 'disclaimers',                     action: 'index', roles: [o, h, r],    icon_name: 'update'            },
      { controller: 'executing_works',                 action: 'index', roles: [o],          icon_name: 'gavel',               information_standard_id: Config[:dynamical_information_standards][:executing_works] },
      { controller: 'resources_to_private_recipients', action: 'index', roles: [o],          icon_name: 'attach_money',        information_standard_id: Config[:dynamical_information_standards][:resources_to_private_recipients] },
      { controller: 'remunerations',                   action: 'index', roles: [o, h, r],    icon_name: 'attach_money',        information_standard_id: Config[:dynamical_information_standards][:institution_remunerations] },
      { controller: 'work_lines',                      action: 'index', roles: [o, h, r],    icon_name: 'attach_money',        information_standard_id: Config[:dynamical_information_standards][:institution_remunerations] },
      { controller: 'budgetary_units',                 action: 'index', roles: [o, h, r],    icon_name: 'attach_money',        information_standard_id: Config[:dynamical_information_standards][:institution_remunerations] },
      { controller: 'institution_recruitment_results', action: 'index', roles: [o],          icon_name: 'account_balance',     information_standard_id: Config[:dynamical_information_standards][:institution_recruitment_results] },
      { controller: 'services',                        action: 'index', roles: [o, h, r],    icon_name: 'room_service',        information_standard_id: Config[:dynamical_information_standards][:institution_services] },
      { controller: 'services_categories',             action: 'index', roles: [o, h, r],    icon_name: 'room_service',        information_standard_id: Config[:dynamical_information_standards][:institution_services] },
      { controller: 'service_steps',                   action: 'index', roles: [o, h, r],    icon_name: 'room_service',        information_standard_id: Config[:dynamical_information_standards][:institution_services] },
      { controller: 'travels',                         action: 'index', roles: [o],          icon_name: 'airplanemode_active', information_standard_id: Config[:dynamical_information_standards][:institution_travels] },
      { controller: 'recycler_docks',                  action: 'index', roles: [a, o, h, r], icon_name: 'delete_sweep'      },
      { controller: 'statistics',                      action: 'index', roles: [a, o, h, r], icon_name: 'pie_chart'         }
    ]

    menus.each do |menu|
      m = Menu.where(controller: menu[:controller]).first_or_initialize
      m.attributes = menu
      m.save
    end
  end
end
