# frozen_string_literal: true
#
namespace :procurements do
  desc 'Charge year column from start_date'
  task load_years: :environment do
    Procurement.where(year: 0).each do |p|
      if p.start_date.is_a?(Date)
        year = p.start_date.year
        p.update_column(:year, year) if year > 2005 && year < 2018
      end
    end
  end
end
