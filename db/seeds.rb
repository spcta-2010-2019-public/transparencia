# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# require 'csv'
# CSV.open("#{Rails.root.to_s}/db/documents.csv", "w") do |csv|
#   csv << [
#     'Acrónimo de Institución',
#     'Nombre de documento',
#     'Fecha de creación',
#     'Enlace'
#   ]
#   Document.includes(:institution).where(enabled: true).where("document_file_name like '%.csv' or document_file_name like '%.xls' or document_file_name like '%.xlsx' or document_file_name like '%.ods'").each do |doc|
#     if doc.document.present?
#       csv << [
#         doc.institution.try(:acronym),
#         doc.name,
#         doc.created_at.try(:strftime, '%d/%m/%Y'),
#         "http://www.transparencia.gob.sv#{doc.document.url(:original, timestamp: false)}"
#       ]
#     end
#   end; nil
# end

# require 'csv'
# CSV.open("#{Rails.root.to_s}/db/documents.csv", "w") do |csv|
#   csv << [
#     'Acrónimo de Institución',
#     'Nombre de documento',
#     'Fecha de creación',
#     'Enlace'
#   ]
#   Document.includes(:institution).where(enabled: true).where("document_file_name like '%.csv' or document_file_name like '%.xls' or document_file_name like '%.xlsx' or document_file_name like '%.ods'").each do |doc|
#     if doc.document.present?
#       csv << [
#         doc.institution.try(:acronym),
#         doc.name,
#         doc.created_at.try(:strftime, '%d/%m/%Y'),
#         "http://www.transparencia.gob.sv#{doc.document.url(:original, timestamp: false)}"
#       ]
#     end
#   end; nil
# end

# require 'csv'
# CSV.open("#{Rails.root.to_s}/db/nat_remuneraciones.csv", "w") do |csv|
#   csv << [
#     'Acrónimo de la institución',
#     'Nombre de la institución',
#     'Año',
#     'Cargo presupuestario',
#     'No Empleados en el cargo',
#     'Remuneración mensual por empleado',
#     'Dietas o gasto de representación',
#     'Categoría salarial',
#   ]
#   Remuneration.includes(:institution).where(enabled: true).order(institution_id: :asc, year: :desc, name: :asc).each do |obj|
#     csv << [
#       obj.institution.try(:acronym),
#       obj.institution.try(:name),
#       obj.year,
#       obj.name,
#       obj.employees_number,
#       obj.monthly_remuneration,
#       obj.expenses_remuneration,
#       obj.salary_category,
#     ]
#   end; nil
# end
#
# CSV.open("#{Rails.root.to_s}/db/nat_viajes.csv", "w") do |csv|
#   csv << [
#     'Acrónimo de la institución',
#     'Nombre de la institución',
#     'Año',
#     'País/Lugar',
#     'Nombre del evento',
#     'Objetivos',
#     'Funcionario',
#     'Cargo',
#     'Fecha de salida',
#     'Fecha de entrada',
#     'Costo Boleto',
#     'Fuente de financiamiento costo boleto',
#     'Viáticos',
#     'Fuente de financiamiento viáticos',
#     'Otros gastos',
#     'Fuente de financiamiento otros gastos',
#   ]
#   Travel.includes(:institution).where(enabled: true).order(institution_id: :asc, departure_date: :desc, name: :asc).each do |obj|
#     csv << [
#       obj.institution.try(:acronym),
#       obj.institution.try(:name),
#       obj.departure_date.try(:year),
#       obj.destination,
#       obj.name,
#       obj.objective,
#       obj.officer_name,
#       obj.officer_position,
#       obj.departure_date.try(:strftime, '%d/%m/%Y'),
#       obj.return_date.try(:strftime, '%d/%m/%Y'),
#       obj.passage_amount,
#       obj.passage_payer,
#       obj.viatical_amount,
#       obj.viatical_payer,
#       obj.expenses_amount,
#       obj.expenses_payer,
#     ]
#   end; nil
# end
#
# CSV.open("#{Rails.root.to_s}/db/nat_recursos_a_privados.csv", "w") do |csv|
#   csv << [
#     'Acrónimo de la institución',
#     'Nombre de la institución',
#     'Dependencía',
#     'Fase de ejecución',
#     'Inicio de ejecución',
#     'Fin de ejecución',
#     'Destinatario privado',
#     'Destino de los recursos (Objetivo)',
#     'Monto de los recursos entregados',
#     'Fuente de financiamiento',
#     'Enlace del documento del informe',
#     'Enlace del documento de la asignación',
#   ]
#   ResourcesToPrivateRecipient.includes(:institution, :institution_dependency).where(enabled: true).order(institution_id: :asc, period_begin: :desc, name: :asc).each do |obj|
#     csv << [
#       obj.institution.try(:acronym),
#       obj.institution.try(:name),
#       obj.institution_dependency.try(:name),
#       obj.execution_phase_string,
#       obj.period_begin.try(:strftime, '%d/%m/%Y'),
#       obj.period_end.try(:strftime, '%d/%m/%Y'),
#       obj.name,
#       obj.allocation_of_resources,
#       obj.resources_amount,
#       obj.funding_source_string,
#       obj.attachment_url,
#       obj.funds_attachment_url,
#     ]
#   end; nil
# end


require 'csv'
CSV.open("#{Rails.root.to_s}/db/catalogo_instituciones.csv", "w") do |csv|
  csv << [
    'Tipo de institución',
    'Acrónimo de Institución',
    'Nombre de institución'
  ]
  Institution.includes(:institution_type).order(:institution_type_id, :name).each do |obj|
    csv << [
      obj.institution_type.try(:name),
      obj.try(:acronym),
      obj.name,
    ]
  end; nil
end
