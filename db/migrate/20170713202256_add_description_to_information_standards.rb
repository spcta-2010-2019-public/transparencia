class AddDescriptionToInformationStandards < ActiveRecord::Migration[5.0]
  def change
    add_column :information_standards, :description, :text
  end
end
