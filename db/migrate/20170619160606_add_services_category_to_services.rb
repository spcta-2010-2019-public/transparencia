class AddServicesCategoryToServices < ActiveRecord::Migration[5.0]
  def change
    add_reference :services, :services_category, foreign_key: true
  end
end
