class CreateInstitutionItems < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_items do |t|
      t.boolean :enabled, null: false, default: false
      t.belongs_to :institution_inventory_category
      t.string :name
      t.text :description
      t.string :brand
      t.decimal  :market_value, :precision => 10, :scale => 2
      t.decimal  :current_value, :precision => 10, :scale => 2
      t.timestamps
    end
  end
end
