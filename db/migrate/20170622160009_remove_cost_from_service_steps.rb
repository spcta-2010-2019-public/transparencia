class RemoveCostFromServiceSteps < ActiveRecord::Migration[5.0]
  def change
    remove_column :service_steps, :cost, :decimal, precision: 12, scale: 2, null: false, default: 0.0
  end
end
