class ChangePriorityInDocuments < ActiveRecord::Migration[5.0]
  def up
    change_column :documents, :priority, :integer, null: true, default: 0
  end

  def down
    change_column :documents, :priority, :integer, null: false, default: 0
  end
end
