class CreateExecutingWorks < ActiveRecord::Migration[5.0]
  def change
    create_table :executing_works do |t|
      t.references :institution, foreign_key: true
      t.boolean :active, null: false, default: true
      t.boolean :enabled, null: false, default: true
      t.boolean :marked, null: false, default: false
      t.date :starting_date
      t.decimal :amount, precision: 12, scale: 2, null: false, default: 0.0
      t.integer :beneficiaries_number, null: false, default: 0
      t.integer :year, null: false, default: 0
      t.string :executing_company, null: false, default: ''
      t.string :execution_time, null: false, default: ''
      t.string :financing_source, null: false, default: ''
      t.string :location, null: false, default: ''
      t.string :name, null: false, default: ''
      t.string :responsible, null: false, default: ''
      t.string :supervising_company, null: false, default: ''
      t.text :contract_code
      t.text :guarantee
      t.text :payment_method

      t.timestamps
    end
  end
end
