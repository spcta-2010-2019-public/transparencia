class AddCostToServices < ActiveRecord::Migration[5.0]
  def change
    add_column :services, :cost, :string, null: false, default: ''
  end
end
