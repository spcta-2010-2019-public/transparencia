class AddFieldsToInstitutionItemsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :institution_items, :institution_id, :integer
    add_column :institution_items, :year, :integer
    remove_column :institution_items, :institution_inventory_category_id, :integer
  end
end
