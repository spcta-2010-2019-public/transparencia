class AddEnabledToParticipationMechanisms < ActiveRecord::Migration[5.0]
  def change
    add_column :participation_mechanisms, :enabled, :boolean, null: false, default: false
  end
end
