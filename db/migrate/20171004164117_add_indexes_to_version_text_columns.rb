class AddIndexesToVersionTextColumns < ActiveRecord::Migration[5.0]
  def up
    execute 'CREATE INDEX versions_object_idx ON versions USING GIN(object gin_trgm_ops);'
    execute 'CREATE INDEX versions_object_changes_idx ON versions USING GIN(object_changes gin_trgm_ops);'
  end

  def down
    execute 'DROP INDEX versions_object_idx;'
    execute 'DROP INDEX versions_object_changes_idx;'
  end
end
