class CreateDocumentCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :document_categories do |t|
      t.references :institution, foreign_key: true
      t.references :information_standard, foreign_key: true
      t.integer :priority, null: false, default: 0
      t.string :name, null: false, default: ''

      t.timestamps
    end
  end
end
