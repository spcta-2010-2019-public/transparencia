class CreateInstitutionDependencies < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_dependencies do |t|
      t.references :institution, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
