class AddPriorityToProcurements < ActiveRecord::Migration[5.0]
  def change
    add_column :procurements, :priority, :integer, null: false, default: 0
  end
end
