class AddYearToRemunerations < ActiveRecord::Migration[5.0]
  def change
    add_column :remunerations, :year, :integer, null: false, default: 0
  end
end
