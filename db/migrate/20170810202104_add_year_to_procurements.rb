class AddYearToProcurements < ActiveRecord::Migration[5.0]
  def change
    add_column :procurements, :year, :integer, null: false, default: 0
  end
end
