class CreateInformationStandardsStandardCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :information_standards_standard_categories, id: false do |t|
      t.references :standard_category, foreign_key: true, index: { name: 'isit_standard_category' }
      t.references :information_standard, foreign_key: true, index: { name: 'isit_information_standard' }
    end
  end
end
