class CreateCommittees < ActiveRecord::Migration[5.0]
  def change
    create_table :committees do |t|
      t.references :institution, foreign_key: true
      t.string :name, null: false, default: ''

      t.timestamps
    end
  end
end
