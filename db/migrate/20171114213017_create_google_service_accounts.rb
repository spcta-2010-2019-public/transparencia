class CreateGoogleServiceAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :google_service_accounts do |t|
      t.text :access_token
      t.datetime :expires_in

      t.timestamps
    end
  end
end
