class AddPayersColumnsToTravels < ActiveRecord::Migration[5.0]
  def change
    add_column :travels, :passage_payer, :integer
    add_column :travels, :viatical_payer, :integer
    add_column :travels, :expenses_payer, :integer

    add_column :travels, :passage_payer_comments, :string, null: false, default: ''
    add_column :travels, :viatical_payer_comments, :string, null: false, default: ''
    add_column :travels, :expenses_payer_comments, :string, null: false, default: ''
  end
end
