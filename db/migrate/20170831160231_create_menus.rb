class CreateMenus < ActiveRecord::Migration[5.0]
  def change
    create_table :menus do |t|
      t.string :controller, null: false, default: ''
      t.string :action, null: false, default: ''
      t.integer :information_standard_id, null: false, default: 0
      t.text :roles
      t.string :icon_name, null: false, default: ''

      t.timestamps
    end
  end
end
