class AddEnabledToResourcesToPrivateRecipients < ActiveRecord::Migration[5.0]
  def change
    add_column :resources_to_private_recipients, :enabled, :boolean, null: false, default: false
  end
end
