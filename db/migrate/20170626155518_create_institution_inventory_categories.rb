class CreateInstitutionInventoryCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_inventory_categories do |t|
      t.boolean :enabled, null: false, default: false
      t.belongs_to :institution
      t.integer :year
      t.timestamps
    end
  end
end
