class CreateRemunerations < ActiveRecord::Migration[5.0]
  def change
    create_table :remunerations do |t|
      t.references :institution, foreign_key: true
      t.references :work_line, foreign_key: true
      t.boolean :active, null: false, default: false
      t.boolean :enabled, null: false, default: false
      t.boolean :marked, null: false, default: false
      t.boolean :ad_honorem, null: false, default: false
      t.string :name, null: false, default: ''
      t.integer :employees_number, null: false, default: 0
      t.decimal :monthly_remuneration, precision: 12, scale: 2, null: false, default: 0.0
      t.decimal :overtime_remuneration, precision: 12, scale: 2, null: false, default: 0.0
      t.decimal :expenses_remuneration, precision: 12, scale: 2, null: false, default: 0.0
      t.string :salary_category, null: false, default: ''

      t.timestamps
    end
  end
end
