class AddEnabledInstitutionRecruitmentResult < ActiveRecord::Migration[5.0]
  def change
    add_column :institution_recruitment_results, :enabled, :boolean, null: false, default: false
  end
end
