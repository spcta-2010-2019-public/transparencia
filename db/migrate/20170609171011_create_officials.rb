class CreateOfficials < ActiveRecord::Migration[5.0]
  def change
    create_table :officials do |t|
      t.references :institution, foreign_key: true
      t.references :institution_dependency, foreign_key: true
      t.references :committee, foreign_key: true
      t.boolean :enabled, null: false, default: false
      t.boolean :active, null: false, default: false
      t.boolean :marked, null: false, default: false
      t.string :name, null: false, default: ''
      t.string :position, null: false, default: ''
      t.string :phone, null: false, default: ''
      t.string :email, null: false, default: ''
      t.text :address
      t.text :functions
      t.text :curriculum

      t.timestamps
    end
  end
end
