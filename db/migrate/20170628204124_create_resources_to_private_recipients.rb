class CreateResourcesToPrivateRecipients < ActiveRecord::Migration[5.0]
  def change
    create_table :resources_to_private_recipients do |t|
      t.integer :institution_id, null: false
      t.integer :institution_dependence_id
      t.string :name, null: false
      t.text :allocation_of_resources, default: nil
      t.decimal :resources_amount, null: false, default: 0, precision: 12, scale: 4
      t.date :period_begin
      t.date :period_end
      t.attachment :funds_attachment
      t.timestamps
    end
  end
end
