class AddSlugToInstitutions < ActiveRecord::Migration[5.0]
  def change
    add_column :institutions, :slug, :string#, null: false
    add_index  :institutions, :slug, unique: true
  end
end
