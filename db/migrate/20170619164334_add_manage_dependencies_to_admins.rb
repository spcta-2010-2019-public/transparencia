class AddManageDependenciesToAdmins < ActiveRecord::Migration[5.0]
  def change
    add_column :admins, :manage_dependencies, :boolean, null: false, default: false
  end
end
