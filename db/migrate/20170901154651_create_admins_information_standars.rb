class CreateAdminsInformationStandars < ActiveRecord::Migration[5.0]
  def change
    create_table :admins_information_standars, id: false do |t|
      t.references :admin, foreign_key: true
      t.references :information_standard, foreign_key: true
    end
  end
end
