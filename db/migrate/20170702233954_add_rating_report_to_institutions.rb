class AddRatingReportToInstitutions < ActiveRecord::Migration[5.0]
  def change
    add_attachment :institutions, :rating_report

  end
end
