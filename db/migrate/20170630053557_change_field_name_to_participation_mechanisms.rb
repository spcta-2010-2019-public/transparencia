class ChangeFieldNameToParticipationMechanisms < ActiveRecord::Migration[5.0]
  def change
    add_column :participation_mechanisms, :institution_dependency_id, :integer
    remove_column :participation_mechanisms, :institution_dependence_id, :integer
  end
end
