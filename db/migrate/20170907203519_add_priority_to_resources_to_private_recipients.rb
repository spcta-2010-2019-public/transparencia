class AddPriorityToResourcesToPrivateRecipients < ActiveRecord::Migration[5.0]
  def change
    add_column :resources_to_private_recipients, :priority, :integer, null: false, default: 0
  end
end
