class CreateInformationStandardStatistics < ActiveRecord::Migration[5.0]
  def change
    create_table :information_standard_statistics do |t|
      t.integer :institution_id, null: false
      t.integer :information_standard_id
      t.integer :sessions, null: false, default: 0
      t.integer :views, null: false, default: 0
      t.date :tracked_date, null: false
      t.timestamps
    end
  end
end
