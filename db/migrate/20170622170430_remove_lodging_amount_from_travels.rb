class RemoveLodgingAmountFromTravels < ActiveRecord::Migration[5.0]
  def change
    remove_column :travels, :lodging_amount, :decimal, precision: 12, scale: 2, null: false, default: 0.0
  end
end
