class AddAssignedUnitToConsultants < ActiveRecord::Migration[5.0]
  def change
    add_column :consultants, :assigned_unit, :string
  end
end
