class AddSocialLinksToInstitutions < ActiveRecord::Migration[5.0]
  def change
    add_column :institutions, :flickr_username, :string, null: false, default: ''
    add_column :institutions, :flickr_url, :string, null: false, default: ''
    add_column :institutions, :youtube_username, :string, null: false, default: ''
    add_column :institutions, :youtube_url, :string, null: false, default: ''
  end
end
