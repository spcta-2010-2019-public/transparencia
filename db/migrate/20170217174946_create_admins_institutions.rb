# frozen_string_literal: true
class CreateAdminsInstitutions < ActiveRecord::Migration[5.0]
  def change
    create_table :admins_institutions, id: false do |t|
      t.references :admin, foreign_key: true
      t.references :institution, foreign_key: true
    end
  end
end
