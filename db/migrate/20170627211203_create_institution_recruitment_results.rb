class CreateInstitutionRecruitmentResults < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_recruitment_results do |t|
      t.belongs_to :institution
      t.string :name, null: false
      t.string :job_url
      t.timestamps
    end
  end
end
