class AddInstitutionDependencyToExecutingWorks < ActiveRecord::Migration[5.0]
  def change
    add_reference :executing_works, :institution_dependency, foreign_key: true
  end
end
