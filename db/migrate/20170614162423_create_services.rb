class CreateServices < ActiveRecord::Migration[5.0]
  def change
    create_table :services do |t|
      t.references :institution, foreign_key: true
      t.references :institution_dependency, foreign_key: true
      t.boolean :active, null: false, default: false
      t.boolean :enabled, null: false, default: false
      t.boolean :marked, null: false, default: false
      t.string :name, null: false, default: ''
      t.string :response_time, null: false, default: ''
      t.string :responsible_area, null: false, default: ''
      t.string :responsible_name, null: false, default: ''
      t.text :description
      t.text :requirements
      t.text :observations
      t.text :address
      t.text :schedule

      t.timestamps
    end
  end
end
