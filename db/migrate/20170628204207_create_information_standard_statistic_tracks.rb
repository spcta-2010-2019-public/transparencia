class CreateInformationStandardStatisticTracks < ActiveRecord::Migration[5.0]
  def change
    create_table :information_standard_statistic_tracks do |t|
      t.integer :institution_id, null: false
      t.integer :information_standard_id, null: false
      t.string :session_id, null: false
      t.date :tracked_date
      t.timestamps
    end
  end
end
