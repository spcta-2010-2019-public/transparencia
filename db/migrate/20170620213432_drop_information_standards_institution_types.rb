class DropInformationStandardsInstitutionTypes < ActiveRecord::Migration[5.0]
  def up
    drop_table :information_standards_institution_types
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
