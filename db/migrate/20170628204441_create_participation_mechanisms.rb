class CreateParticipationMechanisms < ActiveRecord::Migration[5.0]
  def change
    create_table :participation_mechanisms do |t|
      t.integer :institution_dependence_id
      t.text :description
      t.text :participation_requirements
      t.date :execution_date_begin
      t.date :execution_date_end
      t.timestamps
    end
  end
end
