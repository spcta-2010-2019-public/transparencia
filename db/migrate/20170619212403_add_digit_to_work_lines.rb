class AddDigitToWorkLines < ActiveRecord::Migration[5.0]
  def change
    add_column :work_lines, :digit, :integer
  end
end
