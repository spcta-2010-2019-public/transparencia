class AddActiveToProcurements < ActiveRecord::Migration[5.0]
  def change
    add_column :procurements, :enabled, :boolean, null: false, default: false
  end
end
