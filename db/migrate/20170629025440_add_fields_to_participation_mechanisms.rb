class AddFieldsToParticipationMechanisms < ActiveRecord::Migration[5.0]
  def change
    add_column :participation_mechanisms, :objetive, :string 
    add_column :participation_mechanisms, :result, :text

    change_table :participation_mechanisms do |t|
      t.attachment :report
    end
  end
end
