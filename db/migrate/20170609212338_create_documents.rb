class CreateDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :documents do |t|
      t.references :institution, foreign_key: true
      t.references :institution_dependency, foreign_key: true
      t.references :information_standard, foreign_key: true
      t.boolean :enabled, null: false, default: false
      t.boolean :active, null: false, default: false
      t.boolean :marked, null: false, default: false
      t.integer :priority, null: false, default: 0
      t.integer :year, null: false, default: 0
      t.string :name, null: false, default: ''
      t.text :description
      t.attachment :document

      t.timestamps
    end
  end
end
