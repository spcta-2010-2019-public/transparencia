class CreateInstitutionHistoricalConfidentialInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_historical_confidential_infos do |t|
      t.belongs_to :institution_confidential_info, index: {:name => "confidential_info_id"}
      t.text    :comment
      t.date    :last_date
      t.timestamps
    end
  end
end
