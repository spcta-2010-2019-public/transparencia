class CreateInstitutionConfidentialInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :institution_confidential_infos do |t|
      t.boolean :enabled, null: false, default: false
      t.belongs_to :institution
      t.string :declaration, null: false
      t.text :category, null: false
      t.string :admin_unit
      t.date :classification_date
      t.string :classification_type
      t.text :legal_foundation
      t.text :justification
      t.date :declassification_date
      t.string :status
      t.timestamps
    end
  end
end
