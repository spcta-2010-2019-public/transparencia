# frozen_string_literal: true
class CreateInstitutions < ActiveRecord::Migration[5.0]
  def change
    create_table :institutions do |t|
      t.string :name
      t.string :acronym
      t.attachment :avatar

      t.timestamps
    end
  end
end
