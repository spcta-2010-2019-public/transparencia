class AddSlugToInformationStandards < ActiveRecord::Migration[5.0]
  def change
    add_column :information_standards, :slug, :string#, null: false
    add_index  :information_standards, :slug, unique: true
  end
end
