class CreateServiceSteps < ActiveRecord::Migration[5.0]
  def change
    create_table :service_steps do |t|
      t.references :service, foreign_key: true
      t.integer :correlative, null: false, default: 0
      t.string :name, null: false, default: ''
      t.string :responsible_name, null: false, default: ''
      t.string :duration, null: false, default: ''
      t.string :response_time, null: false, default: ''
      t.decimal :cost, precision: 12, scale: 2, null: false, default: 0.0
      t.text :description
      t.text :schedule
      t.text :requirements
      t.text :documents_to_present

      t.timestamps
    end
  end
end
