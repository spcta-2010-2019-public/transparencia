class AddFieldsToInstitutions < ActiveRecord::Migration[5.0]
  def change
    add_reference :institutions, :institution_type, foreign_key: true
    add_reference :institutions, :standard_category, foreign_key: true
    add_column :institutions, :enabled, :boolean, null: false, default: false
    add_column :institutions, :accepts_online_requests, :boolean, null: false, default: false
    add_column :institutions, :administrative_document_type, :integer, null: false, default: 0
    add_column :institutions, :certification_amount, :decimal, precision: 12, scale: 2, null: false, default: 0.0
    add_column :institutions, :reproduction_amount, :decimal, precision: 12, scale: 2, null: false, default: 0.0
    add_column :institutions, :external_transparency_site_url, :string, null: false, default: ''
    add_column :institutions, :facebook_url, :string, null: false, default: ''
    add_column :institutions, :facebook_username, :string, null: false, default: ''
    add_column :institutions, :officer_email, :string, null: false, default: ''
    add_column :institutions, :officer_name, :string, null: false, default: ''
    add_column :institutions, :twitter_url, :string, null: false, default: ''
    add_column :institutions, :twitter_username, :string, null: false, default: ''
    add_column :institutions, :website_url, :string, null: false, default: ''
    add_column :institutions, :officer_designation_date, :date, null: true, default: nil
    add_attachment :institutions, :administrative_document
    add_attachment :institutions, :officer_designation
  end
end
