class CreateWorkLines < ActiveRecord::Migration[5.0]
  def change
    create_table :work_lines do |t|
      t.references :budgetary_unit, foreign_key: true
      t.string :code, null: false, default: ''
      t.string :name, null: false, default: ''

      t.timestamps
    end
  end
end
