#
class AddPriorityOrderToInformationStandards < ActiveRecord::Migration[5.0]
  def change
    add_column :information_standards, :priority_order, :boolean, null: false, default: false
  end
end
