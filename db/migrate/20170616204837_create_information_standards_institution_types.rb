class CreateInformationStandardsInstitutionTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :information_standards_institution_types, id: false do |t|
      t.references :institution_type, foreign_key: true, index: { name: 'isit_institution_type' }
      t.references :information_standard, foreign_key: true, index: { name: 'isit_information_standard' }
    end
  end
end
