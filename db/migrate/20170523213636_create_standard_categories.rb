class CreateStandardCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :standard_categories do |t|
      t.string :name, null: false, default: ''

      t.timestamps
    end
  end
end
