class ChangeInformationStandardsStandardCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :information_standards_standard_categories, :id, :primary_key
    add_column :information_standards_standard_categories, :linked_institution_id, :integer, index: true
    add_foreign_key :information_standards_standard_categories, :institutions, column: :linked_institution_id, name: 'isit_linked_institution'
  end
end
