class ChangeFieldNameToResourcesToPrivateRecipients < ActiveRecord::Migration[5.0]
  def change
    add_column :resources_to_private_recipients, :institution_dependency_id, :integer
    remove_column :resources_to_private_recipients, :institution_dependence_id, :integer
  end
end
