class CreateConsultants < ActiveRecord::Migration[5.0]
  def change
    create_table :consultants do |t|
      t.references :institution, foreign_key: true
      t.references :institution_dependency, foreign_key: true
      t.boolean :enabled, null: false, default: false
      t.boolean :active, null: false, default: false
      t.boolean :marked, null: false, default: false
      t.string :name, null: false, default: ''
      t.string :position, null: false, default: ''
      t.decimal :remuneration, precision: 12, scale: 2, null: false, default: 0.0
      t.string :phone, null: false, default: ''
      t.string :email, null: false, default: ''
      t.text :description
      t.text :applied_studies
      t.text :work_experience
      t.text :functions

      t.timestamps
    end
  end
end
