class AddFieldsToResoucesToPrivateRecipients < ActiveRecord::Migration[5.0]
  def change
    add_column :resources_to_private_recipients, :execution_phase, :integer, null: false, default: 0
    add_column :resources_to_private_recipients, :funding_source, :string
  end
end
