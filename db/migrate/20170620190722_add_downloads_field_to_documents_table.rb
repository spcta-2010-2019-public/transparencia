class AddDownloadsFieldToDocumentsTable < ActiveRecord::Migration[5.0]
  def change
    add_column :documents, :downloads, :integer, default: 0
  end
end
