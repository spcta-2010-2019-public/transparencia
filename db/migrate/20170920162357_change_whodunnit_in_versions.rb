class ChangeWhodunnitInVersions < ActiveRecord::Migration[5.0]
  def up
    execute <<-SQL
      ALTER TABLE versions ALTER COLUMN whodunnit TYPE integer USING (whodunnit::integer);
    SQL
  end

  def down
    change_column :versions, :whodunnit, :string
  end
end
