class AddPriorityToOfficials < ActiveRecord::Migration[5.0]
  def change
    add_column :officials, :priority, :integer, null: false, default: 0
  end
end
