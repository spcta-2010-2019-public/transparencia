class CreateTravels < ActiveRecord::Migration[5.0]
  def change
    create_table :travels do |t|
      t.references :institution, foreign_key: true
      t.references :institution_dependency, foreign_key: true
      t.boolean :enabled, null: false, default: false
      t.boolean :marked, null: false, default: false
      t.string :name, null: false, default: ''
      t.string :officer_name, null: false, default: ''
      t.string :officer_position, null: false, default: ''
      t.string :destination, null: false, default: ''
      t.decimal :passage_amount, precision: 12, scale: 2, null: false, default: 0.0
      t.decimal :viatical_amount, precision: 12, scale: 2, null: false, default: 0.0
      t.decimal :lodging_amount, precision: 12, scale: 2, null: false, default: 0.0
      t.decimal :expenses_amount, precision: 12, scale: 2, null: false, default: 0.0
      t.date :departure_date
      t.date :return_date
      t.text :objective

      t.timestamps
    end
  end
end
