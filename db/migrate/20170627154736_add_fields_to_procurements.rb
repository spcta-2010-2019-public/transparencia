class AddFieldsToProcurements < ActiveRecord::Migration[5.0]
  def change
    add_attachment :procurements, :attachment
    add_column     :procurements, :start_date, :date
    add_column     :procurements, :counterpart_name, :string
  end
end
