class CreateDisclaimers < ActiveRecord::Migration[5.0]
  def change
    create_table :disclaimers do |t|
      t.boolean :enabled, null: false, default: false
      t.belongs_to :institution
      t.belongs_to :institution_dependency
      t.belongs_to :information_standard
      t.string :name
      t.text :description
      t.attachment :file
      t.timestamps
    end
  end
end
