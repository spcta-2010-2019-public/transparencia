class CreateServiceStepAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :service_step_attachments do |t|
      t.references :service_step, foreign_key: true
      t.string :name, null: false, default: ''
      t.text :description
      t.attachment :attachment

      t.timestamps
    end
  end
end
