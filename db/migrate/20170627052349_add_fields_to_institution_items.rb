class AddFieldsToInstitutionItems < ActiveRecord::Migration[5.0]
  def change
    add_column :institution_items, :acquisition_date, :date
    add_attachment :institution_items, :file
  end
end
