class AddDocumentCategoryToDocuments < ActiveRecord::Migration[5.0]
  def change
    add_reference :documents, :document_category, foreign_key: true
  end
end
