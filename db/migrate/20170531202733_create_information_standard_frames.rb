class CreateInformationStandardFrames < ActiveRecord::Migration[5.0]
  def change
    create_table :information_standard_frames do |t|
      t.integer :priority, null: false, default: 0
      t.string :name, null: false, default: ''

      t.timestamps
    end
  end
end
