class AddNameToParticipationMechanisms < ActiveRecord::Migration[5.0]
  def change
    add_column :participation_mechanisms, :name, :string 
  end
end
