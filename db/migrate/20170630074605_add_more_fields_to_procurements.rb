class AddMoreFieldsToProcurements < ActiveRecord::Migration[5.0]
  def change
    add_column :procurements, :institutional_area, :string
    add_column :procurements, :adquisition_code, :string
    add_column :procurements, :contract_code, :string
  end
end
