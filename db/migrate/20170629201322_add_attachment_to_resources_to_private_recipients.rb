class AddAttachmentToResourcesToPrivateRecipients < ActiveRecord::Migration[5.0]
  def change
    add_attachment :resources_to_private_recipients, :attachment
  end
end
