class CreateServicesCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :services_categories do |t|
      t.references :institution, foreign_key: true
      t.string :name, null: false, default: ''

      t.timestamps
    end
  end
end
