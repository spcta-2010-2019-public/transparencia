class CreateProcurements < ActiveRecord::Migration[5.0]
  def change
    create_table :procurements do |t|
      t.integer :institution_id,   null: false
      t.string  :name,             null: false
      t.decimal :amount,           null: false, default: 0, precision: 12, scale: 4
      t.string  :form_of_contract, null: false, default: ''
      t.text    :counterpart_info
      t.text    :compliance_deadlines
      t.timestamps
    end
  end
end
