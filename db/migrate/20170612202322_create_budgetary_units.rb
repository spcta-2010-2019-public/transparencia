class CreateBudgetaryUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :budgetary_units do |t|
      t.references :institution, foreign_key: true
      t.integer :digit, null: false, default: 0
      t.string :name, null: false, default: ''

      t.timestamps
    end
  end
end
