class AddObservationsToTravels < ActiveRecord::Migration[5.0]
  def change
    add_column :travels, :observations, :text
  end
end
