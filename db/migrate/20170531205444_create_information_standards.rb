class CreateInformationStandards < ActiveRecord::Migration[5.0]
  def change
    create_table :information_standards do |t|
      t.references :information_standard_frame, foreign_key: true
      t.integer :priority, null: false, default: 0
      t.string :name, null: false, default: ''
      t.string :short_name, null: false, default: ''

      t.timestamps
    end
  end
end
