class AddInstitutionIdToParticipationMechanism < ActiveRecord::Migration[5.0]
  def change
    add_column :participation_mechanisms, :institution_id, :integer
  end
end
